--[[
Game Hacks

This is a style which allows you to manipulate over 100 different aspects of Hedgewars
using the script parameter in the game scheme.

Read the Game_Hacks_README_v22.txt file to learn how it works.

This file is licensed under the MIT License.
]]

HedgewarsScriptLoad("/Scripts/Locale.lua")
HedgewarsScriptLoad("/Scripts/Params.lua")
HedgewarsScriptLoad("/Scripts/Tracker.lua")

-- Save several gears
local birdyGear = nil
local rcPlaneGear = nil
local saucerGear = nil
local kamikazeGear = nil
local freezerGear = nil
local planeGears = {}
local beeGears = {}
local mineGears = {}
local ropeGear = nil
local portalGears = {}
local teleporterGear = nil
local dynaGears = {}
local healthCaseGearsTodo = {} -- List of health crates for which the health still needs to be set
local hogGears = {}
local pickHammerGear = nil
local hogsLeftSide = {}		--[[ Stores the initial “side” of each hog (true = left, false = right), used for forcefield.
				In this mode, the hegehogs must stay on their side for the rest of the game. ]]
local poisonCloudGears = {}
local tardisGears = {}
local specialJumpingGear = nil	-- Save gear for “no jumping” mode

-- Serveral status variables
local gameStarted = false
local placeHogsPhase = nil	-- Currently in hog placement phase?
local setAirPlaneHealth = false
local hogCounter = 0
local turnsCounter = 0		-- Number of turns played
local roundWind			-- Used for fair wind
local lastRound			-- TotalRounds value for last turn
local sdState = 0		-- Save Sudden Death state (0=not active, 1=active)
local girderPlaced		-- Used for strategic tools
local teams = {}		-- Table indexed by team names, values are number of hogs (at start of game)
local poisonKillDone = false	-- Has the “poison kills” modifier been applied in this turn yet?
local donors = {}		-- Table of hogs which are about to “donate”
local donorBoxes = {}		-- Table of donor boxes, values are tables which contain ammo counts
local flaggedGrave		-- Remember a grave for 1 tick, used by donor boxes
local hogNumbers = {}
local lastConstruction		-- Remember last placed girder or rubber. Used for girder/rubber range
local rangeCircle		-- Used for seduction and resurrector
local manualTimer		-- Used to save the last manual timer used by the user
local setWeaponMessage
local releaseShotInNextTurn	-- Temporary variable for max. launching power
local stuffBuilt = 0		-- How many constructions, rubbers and landsprays have been used this turn (used for constructionlimit)
local extraDamageUsed = false	-- Whether extra damage has been used in this turn
local landCenter = nil		-- x coordinate of the center of the land, used for forcefield
local centerBuffer = 12		-- forcefield: Hedgehogs must stay this far away from the center
local wdGameTicks = 0		-- Helper variable for gravity reset: Stores GameTime
local wdTTL = 0			-- Helper variable for gravity reset: Stores TurnTimeLeft

local tmpSwitchCount
local tmpSelectedAmmoType

-- List of weapons that have a power meter; used by maxPower
-- Longterm TODO: Keep this up-to-date for new Hedgewars releases
local powerWeapons = { amGrenade, amClusterBomb, amBazooka, amBee, amWatermelon,
amHellishBomb, amDrill, amMolotov, amGasBomb, amSMine, amSnowball, amKnife,
amAirMine }

-- More variables grabbed from the engine
local cMaxWindSpeed_QWORD = 1073742
local cMaxPower = 1500

--[[ Parser helpers ]]

local INT_MAX = 2147483647

function minmax(n, min, max)	
	if forceLimits then
		if max and min and max ~= "inf" then
			return math.min(max, math.max(min, n))
		elseif max and max ~= "inf" then
			return math.min(max, n)
		elseif min then
			return math.max(min, n)
		else
			return n
		end
	else
	-- This happens if the “unreasonable values” protection is off
		return n
	end
end

function parseRange(key, str, unitParser, ...)
	if str == nil then return nil end
	local minval, maxval
	local subMin, subMax = string.match(str, "([^-]+)-([^-]+)")
	if subMin ~= nil and subMax ~= nil then
		minval = unitParser(key, subMin, ...)
		maxval = unitParser(key, subMax, ...)
	else
		minval = unitParser(key, str, ...)
		maxval = minval
	end
	if minval ~= nil and maxval ~= nil then
		if minval > maxval then
			minval = nil
			maxval = nil
		end
	end
	return minval, maxval
end


function parsePercentOrNumber(key, str, reference, min, max)
	if str == nil then return nil end

	local n
	min = min or 0
	max = max or INT_MAX
	if str == "inf" or str == "i" then
		return "inf"
	elseif str == "min" then
		return min
	elseif str == "max" then
		return max
	elseif string.sub(str, string.len(str), string.len(str)) == "%" then
		local substr = string.sub(str, 1, string.len(str)-1)
		n = truncStrNum(substr)
		if type(n) == "number" then
			local ret = div(n * reference, 100)
			return minmax(ret, min, max)
		else
			return nil
		end
	else
		n = truncStrNum(str)
		if type(n) == "number" then
			return minmax(n, min, max)
		else
			return nil
		end
	end
end

--[[
- Leave original value if last characters are “ms”
- Multiply by 1000 if last character is “s”
- Leave original value if raw number was supplied
- min and max apply to raw number ]]
function parseTime(key, str, min, max)
	if str == nil then return nil end

	local n
	local len = string.len(str)
	min = min or 0
	max = max or INT_MAX
	-- infinite time
	if str == "inf" or str == "i" then
		return "inf"
	-- minimum
	elseif str == "min" then
		return min
	-- maximum
	elseif str == "max" then
		return max
	-- milliseconds
	elseif string.sub(str, len-1, len) == "ms" then
		local substr = string.sub(str, 1, len-2)
		n = truncStrNum(substr)
	-- seconds
	elseif string.sub(str, len, len) == "s" then
		local substr = string.sub(str, 1, len-1)
		local prePoint, postPoint = string.match(substr, "(%d*)%.(%d*)")
		if postPoint == nil then
			prePoint = string.match(substr, "(%d*)")
			postPoint = "000"
		end
		prePoint = tonumber(prePoint)

		if prePoint == nil then
			return nil
		end

		local postPointN = {}

		for i=1,3 do
			local subN = string.sub(postPoint, i, i)
			if tonumber(subN) ~= nil then
				postPointN[i] = tonumber(subN)
			else
				postPointN[i] = 0
			end
		end

		n = prePoint * 1000

		for i=1,3 do
			n = n + postPointN[i] * math.pow(10, 3-i)
		end



		if type(n) ~= "number" then return nil end
	-- raw number (interpreted as milliseconds)
	else
		n = truncStrNum(str)
	end
	if type(n) ~= "number" then
		return nil
	end
	return minmax(n, min, max)
end

function parseTimeRange(key, str, min, max)
	return parseRange(key, str, parseTime, min, max)
end

function parseInt(key, str, min, max)
	min = min or 0
	max = max or INT_MAX
	if str == "inf" or str == "i" then
		return "inf"
	elseif str == "min" then
		return min
	elseif str == "max" then
		return max
	else
		local n = truncStrNum(str)
		if n ~= nil then
			return minmax(n, min, max)
		else
			return nil
		end
	end
end

function parseIntRange(key, str, min, max)
	return parseRange(key, str, parseInt, min, max)
end

function parseHex(key, str, min, max)
	if str == nil then return nil end
	local s = string.match(str, "(%x*)")
	if s == nil then return nil end
	local n = tonumber("0x"..str)
	if str == "inf" or str == "i" then
		return "inf"
	elseif type(n) == "number" then
		return minmax(n, min, max)
	else
		return nil
	end
end

function parseBool(key, str, default)
	if str == "true" or str == "t" then
		return true
	elseif str == "false" or str == "f" then
		return false
	else
		return default
	end
end

function multiParse(keys, parseFunction, ...)
	for i=1,#keys do
		local result = parseFunction(keys[i], params[keys[i]], ...)
		if result ~= nil then
			return parseFunction(keys[i], params[keys[i]], ...)
		end
	end
	return nil
end

function onParameters()
	parseParams()

	--[[ DEVELOPER NOTE:
	Remove a free letter (for the short form) from this list when you introduce a new parameter with a 1-letter short form.
	Free letters:
		v, V, i
	]]

	-- Script config parameters first
	forceLimits = multiParse({"forcelimits", "Z"}, parseBool, true)
	hedgePot = multiParse({"hedgepot", "H"}, parseBool)

	-- Now for all the other params

	if params["minetimer"] == "random" or params["8"] == "random" or params["minetimer"] == "r" or params["8"] == "r" then
		mineTimerMin = 0
		mineTimerMax = 5000
	else
		mineTimerMin, mineTimerMax = multiParse({"minetimer", "8"}, parseTimeRange, 0)
	end
	if params["stickyminetimer"] == "random" or params["s"] == "random" or params["stickyminetimer"] == "r" or params["s"] == "r" then
		stickyMineTimerMin = 0
		stickyMineTimerMax = 3000
	else
		stickyMineTimerMin, stickyMineTimerMax = multiParse({"stickyminetimer", "s"}, parseTimeRange, 0)
	end
	if params["airminetimer"] == "random" or params["Y"] == "random" or params["airminetimer"] == "r" or params["Y"] == "r" then
		airMineTimerMin = 0
		airMineTimerMax = 1300
	else
		airMineTimerMin, airMineTimerMax = multiParse({"airminetimer", "Y"}, parseTimeRange, 0)
	end
	if params["flamemode"] == "off" or params["flamemode"] == "normal" or params["flamemode"] == "sticky" or params["flamemode"] == "short" then
		flameMode = params["flamemode"]
	elseif params["ef"] == "off" or params["ef"] == "normal" or params["ef"] == "sticky" or params["ef"] == "short" then
		flameMode = params["ef"]
	end

	healthCaseAmountMin, healthCaseAmountMax = multiParse({"healthcratehealth", "eh"}, parseIntRange, 0)
	maxCrateDrops = multiParse({"maxcratedrops", "eC"}, parseInt, 0, 500)
	sturdyCrates = multiParse({"sturdycrates", "ec"}, parseBool)

	gravity = multiParse({"gravity", "g"}, parsePercentOrNumber, 100, 1, 1000)
	-- FIXME: Health is not set to this value when frozen
	dudMineHealth = multiParse({"dudminehealth", "wu"}, parsePercentOrNumber, 36, 1)
	barrelHealthMin, barrelHealthMax = multiParse({"barrelhealth", "b"}, parseIntRange, 1)
	showMineTimer = multiParse({"showminetimer", "im"}, parseBool)
	extraTime = multiParse({"extratime", "e"}, parseTime, 1000)
	saucerFuel = multiParse({"saucerfuel", "f"}, parsePercentOrNumber, 2000, 1, "inf")
	birdyEnergy = multiParse({"birdyenergy", "B"}, parsePercentOrNumber, 2000, 1, "inf")
	landsprayFuel = multiParse({"landsprayfuel", "L"}, parsePercentOrNumber, 1000, 1)
	freezerFuel = multiParse({"freezerfuel", "u"}, parsePercentOrNumber, 1000, 1)
	flamethrowerFuel = multiParse({"flamethrowerfuel", "F"}, parsePercentOrNumber, 500, 1)
	rcPlaneTimer = multiParse({"rcplanetimer", "C"}, parseTime, 1, "inf")
	rcPlaneBombs = multiParse({"rcplanebombs", "R"}, parseInt, 0, "inf")
	birdyEggs = multiParse({"birdyeggs", "E"}, parseInt, 0, "inf")
	pianoBounces = multiParse({"pianobounces", "O"}, parseInt, 1)
	ballGunBalls = multiParse({"ballgunballs", "U"}, parseInt, 1)
	cakeTimer = multiParse({"caketimer", "c"}, parsePercentOrNumber, 2048, 1)
	kamikazeRange = multiParse({"kamikazerange", "K"}, parsePercentOrNumber, 2048, 0)
	kamikazeTrigger = multiParse({"kamikazetrigger", "wk"}, parseBool)
	blowTorchTimer = multiParse({"blowtorchtimer", "T"}, parseTime, 1, "inf")
	pickHammerTimer = multiParse({"pickhammertimer", "I"}, parseTime, 1, "inf")
	planeDrops = multiParse({"planedrops", "4"}, parseInt, 1)
	airAttackBombs = multiParse({"airattackbombs", "A"}, parseInt, 1) or planeDrops
	napalmBombs = multiParse({"napalmbombs", "N"}, parseInt, 1) or planeDrops
	mineStrikeMines = multiParse({"minestrikemines", "M"}, parseInt, 1) or planeDrops
	drillStrikeDrills  = multiParse({"drillstrikedrills", "D"}, parseInt, 1) or planeDrops
	planeDropGap = multiParse({"planedropgap", "2"}, parsePercentOrNumber, 30, 1)
	airAttackGap = multiParse({"airattackgap", "wA"}, parsePercentOrNumber, 30, 1) or planeDropGap
	napalmGap = multiParse({"napalmgap", "wN"}, parsePercentOrNumber, 30, 1) or planeDropGap
	mineStrikeGap = multiParse({"minestrikegap", "wM"}, parsePercentOrNumber, 30, 1) or planeDropGap
	drillStrikeGap = multiParse({"drillstrikegap", "wD"}, parsePercentOrNumber, 30, 1) or planeDropGap
	napalmBombTimer = multiParse({"napalmbombtimer", "tN"}, parseTime, 0)
	-- Maximum is dynamically determined on generation
	girders = multiParse({"girders", "G"}, parseInt, 0)
	ready = multiParse({"ready", "r"}, parseTime, 0, 999000)
	airFlamesHurt = multiParse({"airflameshurt", "a"}, parseBool)
	maxWind = multiParse({"maxwind", "w"}, parsePercentOrNumber, 100, 0, 100)
	lowGravity = multiParse({"lowgravity", "l"}, parsePercentOrNumber, 100, 1, 1000)
	noJumping = multiParse({"nojump", "j"}, parseBool)
	noSnow = multiParse({"nosnow", "Q"}, parseBool)
	-- TODO: Make force field work for more than 2 clan colors
	-- TODO: Make compatible with wrap world edge
	-- TODO: Make compatible with Time Box
	-- FIXME: Hog might get stuck if parachuting into force field
	forceField = multiParse({"forcefield", "ei"}, parseBool)
	portalDistance = multiParse({"portaldistance", "tP"}, parseInt, 0, "inf")
	strategicTools = multiParse({"strategictools", "z"}, parseBool)
	maxPower = multiParse({"maxpower", "hm"}, parseBool)
	shoppaBorder = multiParse({"shoppaborder", "o"}, parseBool)
	poison = multiParse({"poison", "p"}, parseBool)
	epidemic = multiParse({"epidemic", "he"}, parseBool)
	poisonDamage = multiParse({"poisondamage", "P"}, parseInt, 1)
	poisonKills = multiParse({"poisonkills", "n"}, parseBool)
	teamCure = multiParse({"teamcure", "t"}, parseBool)
	shareHealth = multiParse({"sharehealth", "h"}, parseBool)
	davidAndGoliath = multiParse({"davidandgoliath", "d"}, parseBool)
	donorBox = multiParse({"donorbox", "q"}, parseBool)
	fairWind = multiParse({"fairwind", "W"}, parseBool)
	noKnock = multiParse({"noknock", "k"}, parseBool)
	sdOneHP = multiParse({"sd1hp", "1"}, parseBool)
	sdPoison = multiParse({"sdpoison", "S"}, parseBool)
	poisonCloudTimer = multiParse({"poisoncloudtimer", "tp"}, parseTime, 0, 5000)
	beeTimer1 = multiParse({"beedelay", "x"}, parseTime, 1)
	beeTimer2 = multiParse({"beetimer", "y"}, parseTime, 1)
	-- timeboxnotimer is a legacy parameter which is still unofficially supported;
	-- users should now use timeboxturns instead
	tardisReturnEmergencyOnly = multiParse({"timeboxnotimer", "uT"}, parseBool)
	tardisReturnTurns = multiParse({"timeboxturns", "ut"}, parseInt, 0, "inf")
	-- Fall-back condition for the legacy parameter timeboxnotimer
	if(tardisReturnTurns == nil and tardisReturnEmergencyOnly ~= nil) then
		-- legacy timeboxnotimer is the equivalent of timeboxturns=inf
		tardisReturnTurns = "inf"
	end
	pickHammerStraightDown = multiParse({"pickhammerstraightdown", "up"}, parseBool)
	buildLimit = multiParse({"buildlimit", "ub"}, parseInt, 1, "inf")

	if params["balltimer"] == "m" or params["m"] == "m" or params["balltimer"] == "manual" or params["m"] == "manual" then
		ballTimer = "manual"
	else
		ballTimer = multiParse({"balltimer", "m"}, parseTime, 1)
	end
	deagleStrength = multiParse({"deaglestrength", "wd"}, parsePercentOrNumber, 50, 0, 100000)
	sniperStrength = multiParse({"sniperstrength", "ws"}, parsePercentOrNumber, 50, 0, 100000)
	hammerStrength = multiParse({"hammerstrength", "wh"}, parsePercentOrNumber, 125, 1)
	if params["hellishtimer"] == "m" or params["6"] == "m" or params["hellishtimer"] == "manual" or params["6"] == "manual" then
		hhgTimer = "manual"
	else
		hhgTimer = multiParse({"hellishtimer", "6"}, parseTime, 1)
	end
	if params["dynamitetimer"] == "m" or params["5"] == "m" or params["dynamitetimer"] == "manual" or params["5"] == "manual" then
		dynamiteTimer = "manual"
	else
		dynamiteTimer = multiParse({"dynamitetimer", "5"}, parseTime, 0)
	end
	airMineFriction = multiParse({"airminefriction", "wf"}, parsePercentOrNumber, div(cMaxWindSpeed_QWORD*3, 2), 0)
	airMineSeekSpeed = multiParse({"airmineseekspeed", "we"}, parsePercentOrNumber, div(cMaxWindSpeed_QWORD, 2), 0)
	airMineSeekRange = multiParse({"airmineseekrange", "wr"}, parsePercentOrNumber, 175, 0, "inf")
	airMineDamage = multiParse({"airminedamage", "da"}, parsePercentOrNumber, 30, 0)
	if params["drillrockettimer"] == "m" or params["3"] == "m" or params["drillrockettimer"] == "manual" or params["3"] == "manual" then
		drillRocketTimer = "manual"
	else
		drillRocketTimer = multiParse({"drillrockettimer", "3"}, parseTime, 0)
	end

	-- Parameters for 0.9.22 or later
	if SetGearFriction ~= nil then
		hogFriction = multiParse({"hogfriction", "0"}, parseInt, -10, 9989)
	end
	-- 0.9.23 or later
	if SetGearValues ~= nil then
		-- Damage
		mineDamage = multiParse({"minedamage", "d8"}, parsePercentOrNumber, 50, 0, 3000)
		stickyMineDamage = multiParse({"stickyminedamage", "dt"}, parsePercentOrNumber, 30, 0, 3000)
		grenadeDamage = multiParse({"grenadedamage", "dg"}, parsePercentOrNumber, 50, 0, 3000)
		hhgDamage = multiParse({"hellishdamage", "dH"}, parsePercentOrNumber, 90, 0, 3000)
		clusterBombDamage = multiParse({"clusterbombdamage", "dl"}, parsePercentOrNumber, 20, 0, 3000)
		clusterDamage = multiParse({"clusterdamage", "dL"}, parsePercentOrNumber, 25, 0, 3000)
		melonDamage = multiParse({"melondamage", "dm"}, parsePercentOrNumber, 75, 0, 3000)
		melonPieceDamage = multiParse({"melonpiecedamage", "dM"}, parsePercentOrNumber, 75, 0, 3000)
		ballDamage = multiParse({"balldamage", "dU"}, parsePercentOrNumber, 40, 0, 3000)
		bazookaDamage = multiParse({"bazookadamage", "db"}, parsePercentOrNumber, 50, 0, 3000)
		mortarDamage = multiParse({"mortardamage", "do"}, parsePercentOrNumber, 20, 0, 3000)
		beeDamage = multiParse({"beedamage", "dx"}, parsePercentOrNumber, 50, 0, 3000)
		dynamiteDamage = multiParse({"dynamitedamage", "d5"}, parsePercentOrNumber, 75, 0, 3000)
		cakeDamage = multiParse({"cakedamage", "dc"}, parsePercentOrNumber, 75, 0, 3000)
		batDamage = multiParse({"batdamage", "dB"}, parsePercentOrNumber, 30, 0)
		shoryukenDamage = multiParse({"shoryukendamage", "dF"}, parsePercentOrNumber, 30, 0)
		whipDamage = multiParse({"whipdamage", "dw"}, parsePercentOrNumber, 30, 0)
		rcPlaneDamage = multiParse({"rcplanedamage", "dr"}, parsePercentOrNumber, 25, 0, 3000)
		cleaverDamage = multiParse({"cleaverdamage", "dv"}, parsePercentOrNumber, 40000, 0, 3000)
		eggDamage = multiParse({"eggdamage", "dE"}, parsePercentOrNumber, 10, 0, 3000)
		pianoDamage = multiParse({"pianodamage", "dp"}, parsePercentOrNumber, 80, 0, 3000)
		limburgerDamage = multiParse({"limburgerdamage", "di"}, parsePercentOrNumber, 20, 0, 3000)
		deagleDamage = multiParse({"deagledamage", "dd"}, parsePercentOrNumber, 7, 0)
		minigunDamage = multiParse({"minigundamage", "dG"}, parsePercentOrNumber, 2, 0)
		shotgunDamage = multiParse({"shotgundamage", "dO"}, parsePercentOrNumber, 25, 0)
		sniperDamage = multiParse({"sniperdamage", "ds"}, parsePercentOrNumber, 100000, 0)
		sineDamage = multiParse({"sinedamage", "dS"}, parsePercentOrNumber, 35, 0)
		kamikazeDamage = multiParse({"kamikazedamage", "dK"}, parsePercentOrNumber, 30, 0)
		airBombDamage = multiParse({"airbombdamage", "dA"}, parsePercentOrNumber, 30, 0, 3000)
		drillRocketDamage = multiParse({"drillrocketdamage", "d3"}, parsePercentOrNumber, 50, 0, 3000)
		drillStrikeDrillDamage = multiParse({"drillstrikedrilldamage", "dD"}, parsePercentOrNumber, 30, 0, 3000)
		blowTorchDamage = multiParse({"blowtorchdamage", "dT"}, parsePercentOrNumber, 2, 0)
		pickHammerDamage = multiParse({"pickhammerdamage", "dI"}, parsePercentOrNumber, 6, 0)
		hammerDamage = multiParse({"hammerdamage", "dj"}, parseInt, 1)
		hammerExtraDamage = multiParse({"hammerextradamage", "dJ"}, parseInt, 1)
		flameDamage = multiParse({"flamedamage", "df"}, parsePercentOrNumber, 2, 0, 500)
		hogDamage = multiParse({"hogdamage", "dh"}, parsePercentOrNumber, 30, 0, 3000)
		crateDamage = multiParse({"cratedamage", "dC"}, parsePercentOrNumber, 25, 0, 3000)
		barrelDamage = multiParse({"barreldamage", "de"}, parsePercentOrNumber, 75, 0, 3000)

		-- Misc.
		mudballPush = multiParse({"mudballstrength", "wm"}, parsePercentOrNumber, 200000, 0, 20000000)
		hogFreeze = multiParse({"hogfreeze", "hf"}, parsePercentOrNumber, 199999, 0)
		sineStrength = multiParse({"sinestrength", "wS"}, parseInt, 1, 10000)
		resurrectorRange = multiParse({"resurrectorrange", "9"}, parsePercentOrNumber, 100, 0, 40000)
		seductionRange = multiParse({"seductionrange", "7"}, parsePercentOrNumber, 250, 0, 40000)
		ropeOpacity = multiParse({"ropeopacity", "xR"}, parsePercentOrNumber, 255, 0, 255)
		if params["ropecolor"] == "clan" or params["xr"] == "clan" then
			ropeColor = "clan"
		else
			ropeColor = multiParse({"ropecolor", "xr"}, parseHex, 0x0, 0xFFFFFF)
		end
	end
	buildRange = multiParse({"buildrange", "J"}, parsePercentOrNumber, 256, 1, "inf")
end

function onGameInit()
	-- Build desciption text
	local goalArray = {}

	-- Activate the hedgepot
	local hpModWeaps, hpTexts
	if hedgePot == true then
		hpModWeaps, hpTexts = hedgepot()
		-- Text comes later
	end


	-- Most significant game hacks first
	if gravity ~= nil and gravity ~= 100 then
		if gravity > 100 then
			table.insert(goalArray, string.format(loc("High gravity: Base gravity is %d%%."), math.floor(gravity)))
		else
			table.insert(goalArray, string.format(loc("Low gravity: Base gravity is %d%%."), math.floor(gravity)))
		end
		DisableGameFlags(gfLowGravity)
	end
	if strategicTools == true then
		table.insert(goalArray, loc("Strategic tools: Construction, rubber, mud ball, land spray and resurrector ends your turn."))
		SetAmmoTexts(amGirder, nil, nil, nil, false)
		SetAmmoTexts(amRubber, nil, nil, nil, false)
		SetAmmoTexts(amSnowball, nil, nil, nil, false)
		SetAmmoTexts(amLandGun, nil, nil, nil, false)
		SetAmmoTexts(amResurrector, nil, nil, nil, false)
		DisableGameFlags(gfInfAttack)
	end
	if buildLimit == 1 then
		table.insert(goalArray, string.format(loc("Slow builder: You may use only one construction, rubber or land spray per turn."), buildLimit))
	elseif buildLimit ~= nil and buildLimit ~= "inf" then
		table.insert(goalArray, string.format(loc("Slow builder: You may use construction, rubber and land spray up to %d time(s) per turn."), buildLimit))
	end
	if forceField == true then
		table.insert(goalArray, loc("Force field: Hedgehogs can not enter the opposite side of the terrain."))
		if WorldEdge == weWrap then
			WorldEdge = weNone
		end
	end
	if maxPower == true then
		table.insert(goalArray, loc("Maximum launching power: Weapons will always be fired with full power."))
	end
	if buildRange == "inf" then
		if SetMaxBuildDistance ~= nil then
			SetMaxBuildDistance(0)
		end
		table.insert(goalArray, loc("Construction site: Girders and rubber can be placed without range limits."))
	end
	if noJumping == true then
		table.insert(goalArray, loc("No jumping: Hedgehogs can't jump."))
	end
	if noKnock == true then
		table.insert(goalArray, loc("Hogs of Steel: Hedgehogs won't be pushed away by sliding hedgehogs."))
	end
	if hogFriction ~= nil then
		if hogFriction >= 9989 then
			table.insert(goalArray, loc("Extremely sticky hedgehogs: Hedgehogs won't slide at all."))
		elseif hogFriction > 0 then
			table.insert(goalArray, loc("Sticky hedgehogs: Hedgehog won't slide as much."))
		elseif hogFriction < 0 then
			table.insert(goalArray, loc("Slippery hedgehogs: Hedgehog will slide more easily, watch your step!"))
		end
	end

	if sturdyCrates == true then
		table.insert(goalArray, loc("Sturdy crates: Crates can not be destroyed."))
	end
	if maxCrateDrops ~= nil then
		MaxCaseDrops = maxCrateDrops
	end
	if teamCure == true then
		table.insert(goalArray, loc("TeamCure™: Health crates cure all team mates of poison."))
	end
	if shareHealth == true then
		table.insert(goalArray, loc("Health Socialism: Health in crates is shared among team comrades."))
	end
	if epidemic == true then
		table.insert(goalArray, loc("Epidemic: Poisoned hedgehogs infect others by touching."))
	end
	if poisonKills == true then
		table.insert(goalArray, loc("Lethal poison: Poison can reduce the health to 0."))
	end
	if flameMode == "off" then
		table.insert(goalArray, loc("No flames: Flames do not appear"))
		SetAmmoTexts(amHellishBomb, nil, loc("Forged in the depths of hell"), loc("Unleash hell on your foes by using|this fiendish explosive. Don't get too close to it!|Attack: Hold to shoot with more power"))
	elseif flameMode == "sticky" then
		table.insert(goalArray, loc("Long-lived fire: All flames keep burning between turns."))
	elseif flameMode == "short" then
		table.insert(goalArray, loc("Short-lived fire: All flames quickly burn out and do not keep burning between turns."))
	end
	if airFlamesHurt == true and flameMode ~= "off" then
		table.insert(goalArray, loc("Dangerous falling flames: Flames in mid-air can hurt hedgehogs."))
	end
	if donorBox == true then
		if GetGameFlag(gfKing) then
			table.insert(goalArray, loc("Donor boxes: A dying king leaves a box with all the hog's weapons inside."))
		else
			table.insert(goalArray, loc("Donor boxes: The last dying hog of a team leaves a box with all the hog's weapons inside."))
		end
	end

	-- Weapon/utiliy tweaks later

	-- Mines timer
	-- We set MinesTime to 3000 to suppress the engine's message
	if mineTimerMin == 0 and mineTimerMax == 0 then
		table.insert(goalArray, loc("Mine timer: Mines explode instantly."))
		MinesTime = 3000
	elseif mineTimerMin ~= mineTimerMax then
		local str
		if(mineTimerMin % 1000 == 0 and mineTimerMax % 1000 == 0) then
			str = string.format(loc("Mine timer: Mines explode after %.0f-%.0f seconds."), mineTimerMin/1000, mineTimerMax/1000)
		else
			str = string.format(loc("Mine timer: Mines explode after %.1f-%.1f seconds."), mineTimerMin/1000, mineTimerMax/1000)
		end
		table.insert(goalArray, str)
		MinesTime = 3000
	elseif mineTimerMin ~= nil then
		local str
		if(mineTimerMin % 1000 == 0 and mineTimerMax % 1000 == 0) then
			str = string.format(loc("Mine timer: Mines explode after %.0f second(s)."), mineTimerMin/1000)
		else
			str = string.format(loc("Mine timer: Mines explode after %.1f second(s)."), mineTimerMin/1000)
		end
		table.insert(goalArray, str)
		MinesTime = 3000
	end
	if stickyMineTimerMin == 0 and stickyMineTimerMax == 0 then
		table.insert(goalArray, loc("Sticky mine timer: Sticky mines explode instantly."))
	elseif stickyMineTimerMin ~= stickyMineTimerMax then
		local str
		if(stickyMineTimerMin % 1000 == 0 and stickyMineTimerMax % 1000 == 0) then
			str = string.format(loc("Sticky mine timer: Sticky mines explode after %.0f-%.0f seconds."), stickyMineTimerMin/1000, stickyMineTimerMax/1000)
		else
			str = string.format(loc("Sticky mine timer: Sticky mines explode after %.1f-%.1f seconds."), stickyMineTimerMin/1000, stickyMineTimerMax/1000)
		end
		table.insert(goalArray, str)
	elseif stickyMineTimerMin ~= nil then
		local str
		if(stickyMineTimerMin % 1000 == 0 and stickyMineTimerMax % 1000 == 0) then
			str = string.format(loc("Sticky mine timer: Sticky mines explode after %.0f second(s)."), stickyMineTimerMin/1000)
		else
			str = string.format(loc("Sticky mine timer: Sticky mines explode after %.1f second(s)."), stickyMineTimerMin/1000)
		end
		table.insert(goalArray, str)
	end
	if airMineTimerMin == 0 and airMineTimerMax == 0 then
		table.insert(goalArray, loc("Air mine timer: Air mines explode instantly."))
	elseif airMineTimerMin ~= airMineTimerMax then
		local str
		if(airMineTimerMin % 1000 == 0 and airMineTimerMax % 1000 == 0) then
			str = string.format(loc("Air mine timer: Air mines explode after %.0f-%.0f seconds."), airMineTimerMin/1000, airMineTimerMax/1000)
		else
			str = string.format(loc("Air mine timer: Air mines explode after %.1f-%.1f seconds."), airMineTimerMin/1000, airMineTimerMax/1000)
		end
		table.insert(goalArray, str)
	elseif airMineTimerMin ~= nil then
		local str
		if(airMineTimerMin % 1000 == 0 and airMineTimerMax % 1000 == 0) then
			str = string.format(loc("Air mine timer: Air mines explode after %.0f second(s)."), airMineTimerMin/1000)
		else
			str = string.format(loc("Air mine timer: Air mines explode after %.1f second(s)."), airMineTimerMin/1000)
		end
		table.insert(goalArray, str)
	end
	if kamikazeTrigger then
		SetAmmoDescriptionAppendix(amKamikaze, loc("Attack: Detonate early"))
	end
	if saucerFuel == "inf" then
		SetAmmoDescriptionAppendix(amJetpack, "| |" .. loc("Modifications:") .. " |" .. loc("Infinite fuel."))
	elseif saucerFuel ~= nil and saucerFuel ~= 2000 then
		SetAmmoDescriptionAppendix(amJetpack, "| |" .. loc("Modifications:") .. " |" .. string.format(loc("Fuel: %d%%"), round(saucerFuel/20)))
	end
	if birdyEnergy == "inf" then
		SetAmmoDescriptionAppendix(amBirdy, "| |" .. loc("Modifications:") .. " |" .. loc("Birdy can fly forever."))
	elseif birdyEnergy ~= nil then
		SetAmmoDescriptionAppendix(amBirdy, "| |" .. loc("Modifications:") .. " |" .. string.format(loc("Stamina: %d%%"), round(birdyEnergy/20)))
	end
	if rcPlaneTimer == "inf" then
		SetAmmoDescriptionAppendix(amRCPlane, "| |" .. loc("Modifications:") .. " |" .. loc("Infinite fuel."))
	elseif rcPlaneTimer ~= nil and rcPlaneTimer ~= 15000 then
		SetAmmoDescriptionAppendix(amRCPlane, "| |" .. loc("Modifications:") .. " |" .. string.format(loc("Flight time: %d second(s)"), round(rcPlaneTimer/1000)))
	end
	if portalDistance ~= nil and portalDistance ~= "inf" then
		SetAmmoDescriptionAppendix(amPortalGun, "| |" .. loc("Modifications:") .. " |" .. string.format(loc("Maximum portal distance: %d"), portalDistance))
	end
	if lowGravity ~= nil and lowGravity ~= 50 then
		if lowGravity <= 100 then

			table.insert(goalArray, string.format(loc("Modified low gravity: Low gravity utility modifies the gravity to %d%% of the base gravity."), math.floor(lowGravity)))
		else
			table.insert(goalArray, string.format(loc("High gravity utility: Low gravity utility modifies the gravity to %d%% of the base gravity."), math.floor(lowGravity)))
		end
		DisableGameFlags(gfLowGravity)
	end
	if extraTime ~= nil and extraTime ~= 30000 then
		SetAmmoTexts(amExtraTime, nil, nil, string.format(loc("Add %.0f second(s) to your turn time.|Attack: Activate"), extraTime/1000))
	end

	-- Hedgepot: Random modified property ofbweapon/utility property
	if hedgePot and hpModWeaps > 0 then
		-- Assumes that only 1 weapon/utility is changed
		table.insert(goalArray, string.format(loc("Surprise supplies: A random property was changed: %s"), hpTexts[1]))
	end

	-- Concatenate the entire array for the final Goals string
	if #goalArray > 0 then
		local goalString = ""
		for i=1,#goalArray do
			goalString = goalString .. goalArray[i] .. "|"
		end
		Goals = goalString
	end

	-- Misc. setup (no messages)
	if ready ~= nil then
		Ready = ready
	end
	if shoppaBorder == true then
		EnableGameFlags(gfShoppaBorder)
	end
	if fairWind == true or maxWind ~= nil then
		-- Disable engine's wind because we want to do it our own way
		EnableGameFlags(gfDisableWind)
	end
	if(GetGameFlag(gfPlaceHog)) then
		placeHogsPhase = true
	else
		placeHogsPhase = false
	end

	-- Set gravity
	if gravity ~= nil then
		SetGravity(gravity)
	end

	trackTeams()
end

function onAmmoStoreInit()
	for a=0, AmmoTypeMax do
		-- Ban certain ammos with certain settings
		if forceField and a == amTardis then
			SetAmmo(a, 0, 0, 0, 1)
		elseif flameMode == "off" and (a == amFlamethrower or a == amMolotov or a == amNapalm) then
			SetAmmo(a, 0, 0, 0, 1)
		else
			local count, prob, delay, numberInCrate = GetAmmo(a)
			SetAmmo(a, count, prob, delay, numberInCrate)
		end
	end
end

function onGearAdd(gear)
	local gt = GetGearType(gear)

	-- Gear hacks
	if gt == gtHedgehog then
		trackGear(gear)
		hogGears[gear] = true

		local teamName = GetHogTeamName(gear)
		if hogNumbers[teamName] == nil then
			-- first hog
			hogNumbers[teamName] = { [1] = gear }
		else
			table.insert(hogNumbers[teamName], gear)
		end
		if teams[teamName] == nil then
			teams[teamName] = 1
		else
			teams[teamName] = teams[teamName] + 1
		end

		if poison == true then
			local dmg
			if poisonDamage == nil then
				dmg = 5
			else
				dmg = poisonDamage
			end
			SetEffect(gear, hePoisoned, dmg)
		end

		if noKnocking then
			disableKnocking(gear)
		end
		if hogFriction ~= nil then
			-- This assumes a default friction of 9989
			local newFriction = GetGearFriction(gear) - hogFriction
			SetGearFriction(gear, newFriction)
		end
		if hogDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, hogDamage)
		end
	end
	if gt == gtCase then
		if crateDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, crateDamage)
		end
		-- Health case handling
		if (healthCaseAmountMin ~= nil and healthCaseAmountMax ~= nil) or (shareHealth) then
			table.insert(healthCaseGearsTodo, gear)
		end
		if sturdyCrates then
			SetState(gear, bor(GetState(gear), gstNoDamage))
		end
	end
	if gt == gtExplosives then
		if barrelHealthMin ~= nil and barrelHealthMax ~= nil then
			local barrelHealth = barrelHealthMin + GetRandom(barrelHealthMax-barrelHealthMin+1)
			SetHealth(gear, barrelHealth)
			if barrelHealth > 60 then
				SetState(gear, bor(GetState(gear), gstFrozen))
			end
		end
		if barrelDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, barrelDamage)
		end
	end
	if gt == gtShell then
		if bazookaDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, bazookaDamage)
		end
	end
	if gt == gtMortar then
		if mortarDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, mortarDamage)
		end
	end
	if gt == gtGrenade then
		if grenadeDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, grenadeDamage)
		end
	end
	if gt == gtClusterBomb then
		if clusterBombDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, clusterBombDamage)
		end
	end
	if gt == gtCluster then
		if clusterDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, clusterDamage)
		end
	end
	if gt == gtMine then
		mineGears[gear] = true
		if mineTimerMin ~= nil then
			if mineTimerMin ~= mineTimerMax then
				SetTimer(gear, mineTimerMin + GetRandom(mineTimerMax-mineTimerMin+1))
			else
				SetTimer(gear, mineTimerMin)
			end
		end
		if mineDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, mineDamage)
		end
		if showMineTimer then
			SetGearValues(gear, nil, nil, nil, nil, nil, 0)
		end
	end
	if gt == gtSMine then
		if stickyMineTimerMin ~= nil then
			if stickyMineTimerMin ~= stickyMineTimerMax then
				SetTimer(gear, stickyMineTimerMin + GetRandom(stickyMineTimerMax-stickyMineTimerMin+1))
			else
				SetTimer(gear, stickyMineTimerMin)
			end
		elseif stickyMineTimer ~= nil then
			SetTimer(gear, stickyMineTimer)
		end
		if stickyMineDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, stickyMineDamage)
		end
		if showMineTimer then
			SetVisualGearValues(gear, nil, nil, nil, nil, nil, 0)
		end
	end
	if gt == gtAirMine then
		if airMineTimerMin ~= nil then
			local t
			if airMineTimerMin ~= airMineTimerMax then
				t = airMineTimerMin + GetRandom(airMineTimerMax-airMineTimerMin+1)
			else
				t = airMineTimerMin
			end
			SetTimer(gear, t)
			SetGearValues(gear, nil, nil, t)   -- WDTimer
		end
		if airMineSeekSpeed ~= nil or airMineSeekRange ~= nil then
			local r = airMineSeekRange
			if airMineSeekRange == "inf" then
				r = 0xFFFFFFFF
			end
			SetGearValues(gear, r, airMineSeekSpeed)
		end
		if airMineFriction ~= nil then
			SetGearPos(gear, airMineFriction)
		end
		if airMineDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, airMineDamage)   -- Karma
		end
		if showMineTimer then
			SetGearValues(gear, nil, nil, nil, nil, nil, 0)
		end
	end
	if gt == gtJetpack then
		saucerGear = gear
		if type(saucerFuel) == "number" then
			SetHealth(gear, saucerFuel)
		elseif saucerFuel == "inf" then
			SetHealth(gear, JETPACK_FUEL_INFINITE)
		end
	end
	if gt == gtIceGun then
		if freezerFuel ~= nil then
			SetHealth(gear, freezerFuel)
		end
		freezerGear = gear
	end
	if gt == gtFlamethrower then
		if flamethrowerFuel ~= nil then
			SetHealth(gear, flamethrowerFuel)
		end
	end
	if gt == gtLandGun then
		if buildLimit ~= nil and buildLimit ~= "inf" then
			if stuffBuilt >= buildLimit then
				-- Destroy land gun immediately if construction limit is exceeded
				PlaySound(sndDenied)
				AddCaption(loc("Building limit exceeded!"), 0xFFFFFFFF, capgrpAmmostate)
				SetHealth(gear, 0)
				-- This may have reduced ammo by one, so we have to return it
				local ammo = GetAmmoCount(CurrentHedgehog, amLandGun)
				if(ammo ~= 100) then
					AddAmmo(CurrentHedgehog, amLandGun, ammo + 1)
				end
			else
				stuffBuilt = stuffBuilt + 1
				AddCaption(string.format(loc("Buildings left: %d"), buildLimit - stuffBuilt), 0xFFFFFFFF, capgrpAmmostate)
				if stuffBuilt >= buildLimit then
					SetMaxBuildDistance(1)
				end
			end
		end
		if landsprayFuel ~= nil then
			SetHealth(gear, landsprayFuel)
		end
	end
	if gt == gtRCPlane then
		rcPlaneGear = gear
		if type(rcPlaneTimer) == "number" then
			SetTimer(gear, rcPlaneTimer)
		end
		if rcPlaneBombs == "inf" then
			-- Set RC plane bombs to a arbitrarily large number, but later it will be ensured this stays always above 0
			SetHealth(gear, 9001)
		elseif rcPlaneBombs ~= nil then
			SetHealth(gear, rcPlaneBombs)
		end
		if rcPlaneDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, rcPlaneDamage)
		end
	end
	if gt == gtAirBomb then
		if airBombDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, airBombDamage)
		end
	end
	if gt == gtBirdy then
		birdyGear = gear
		if type(birdyEnergy) == "number" then
			SetHealth(gear, birdyEnergy)
		elseif birdyEnergy == "inf" then
			SetHealth(gear, BIRDY_ENERGY_INFINITE)
		end
		if birdyEggs == "inf" then
			-- Set eggs to a arbitrarily large number, but later it will be ensured this stays always above 0
			SetFlightTime(gear, 9001)
		elseif birdyEggs ~= nil then
			SetFlightTime(gear, birdyEggs)
		end
	end
	if gt == gtEgg then
		if eggDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, eggDamage)
		end
	end
	if gt == gtPiano then
		if pianoBounces ~= nil then
			SetGearPos(gear, 5 - pianoBounces)
		end
		if pianoDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, pianoDamage)
		end
	end
	if gt == gtAirAttack then
		planeGears[gear] = true
		local planeType = GetState(gear)
		if planeType == 0 then
			-- air attack
			if airAttackBombs ~= nil then
				SetHealth(gear, airAttackBombs)
			end
			if airAttackGap ~= nil then
				SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, airAttackGap)
			end
		elseif planeType == 1 then
			-- mine strike
			if mineStrikeMines ~= nil then
				SetHealth(gear, mineStrikeMines)
			end
			if mineStrikeGap ~= nil then
				SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, mineStrikeGap)
			end
		elseif planeType == 2 then
			-- napalm
			if napalmBombs ~= nil then
				SetHealth(gear, napalmBombs)
			end
			if napalmGap ~= nil then
				SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, napalmGap)
			end
		elseif planeType == 3 then
			-- drill strike
			if drillStrikeDrills ~= nil then
				SetHealth(gear, drillStrikeDrills)
			end
			if drillStrikeGap ~= nil then
				SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, drillStrikeGap)
			end
		end
	end
	if gt == gtNapalmBomb and napalmBombTimer ~= nil then
		SetTimer(gear, napalmBombTimer)
	end
	if gt == gtBallGun and ballGunBalls ~= nil then
		SetTimer(gear, ballGunBalls * 100 - 99)
	end
	if gt == gtBall then
		if ballTimer ~= nil then
			local timer
			if type(ballTimer) == "number" then
				timer = ballTimer
			elseif ballTimer == "manual" then
				if manualTimer == nil then
					manualTimer = 5
				end
				timer = manualTimer*1000
			end
			SetTimer(gear, timer)
		end
		if ballDamage ~= nil  then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, ballDamage)
		end
	end
	if gt == gtSniperRifleShot then
		if sniperStrength ~= nil then
			SetHealth(gear, sniperStrength)
		end
		if sniperDamage ~= nil  then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, sniperDamage)
		end
	end
	if gt == gtDEagleShot then
		if deagleStrength ~= nil then
			SetHealth(gear, deagleStrength)
		end
		if deagleDamage ~= nil  then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, deagleDamage)
		end
	end
	if gt == gtMinigunBullet then
		if minigunDamage ~= nil  then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, minigunDamage)
		end
	end
	if gt == gtShotgunShot then
		if shotgunDamage ~= nil  then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, shotgunDamage)
		end
	end
	if gt == gtHellishBomb then
		if hhgTimer ~= nil then
			local timer
			if type(hhgTimer) == "number" then
				timer = hhgTimer
			elseif hhgTimer == "manual" then
				if manualTimer == nil then
					manualTimer = 5
				end
				timer = manualTimer*1000
			end
			SetTimer(gear, timer)
		end
		if hhgDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, hhgDamage)
		end
	end
	if gt == gtWatermelon then
		if melonDamage ~= nil  then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, melonDamage)
		end
	end
	if gt == gtMelonPiece then
		if melonPieceDamage ~= nil  then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, melonPieceDamage)
		end
	end
	if gt == gtDynamite then
		if dynamiteTimer ~= nil then
			local timer
			if type(dynamiteTimer) == "number" then
				timer = dynamiteTimer
			elseif dynamiteTimer == "manual" then
				if manualTimer == nil then
					manualTimer = 5
				end
				timer = manualTimer*1000
			end

			SetTimer(gear, timer)
			if timer < 5000 then
				SetTag(gear, div(5000-timer, 166))
			end
		end
		if dynamiteDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, dynamiteDamage)
		end
		dynaGears[gear] = true
	end
	if gt == gtGasBomb then
		if limburgerDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, limburgerDamage)
		end
	end
	if gt == gtPoisonCloud and poisonCloudTimer ~= nil then
		if poisonCloudTimer == 0 then
			DeleteGear(gear)
		else
			SetTimer(gear, poisonCloudTimer)
		end
	end
	if gt == gtSineGunShot then
		if sineStrength ~= nil then
			SetGearValues(gear, nil, nil, nil, sineStrength)
		end
		if sineDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, sineDamage)
		end
	end
	if gt == gtResurrector and resurrectorRange ~= nil then
		SetGearValues(gear, nil, nil, nil, resurrectorRange)
	end
	if gt == gtSeduction and seductionRange ~= nil then
		SetGearValues(gear, nil, nil, nil, seductionRange)
	end
	if gt == gtBee then
		if beeTimer1 ~= nil then
			SetTimer(gear, beeTimer1)
		end
		if beeTimer1 ~= nil or beeTimer2 ~= nil then
			SetGearPos(gear, 0)
		end
		if beeDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, beeDamage)
		end
		beeGears[gear] = true
	end
	if gt == gtDrill then
		-- Is this a normal drill (not from drill strike)?
		if band(GetState(gear), gsttmpFlag) == 0 then
			if drillRocketTimer ~= nil then
				local timer
				if type(drillRocketTimer) == "number" then
					timer = drillRocketTimer
				elseif drillRocketTimer == "manual" then
					if manualTimer == nil then
						manualTimer = 5
					end
					timer = manualTimer*1000
				end
				SetTimer(gear, timer)
			end
			if drillRocketDamage ~= nil then
				SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, drillRocketDamage)
			end
		else
			if drillStrikeDrillDamage ~= nil then
				SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, drillStrikeDrillDamage)
			end
		end
	end
	if gt == gtCake then
		if cakeTimer ~= nil then
			SetHealth(gear, cakeTimer)
		end
		if cakeDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, cakeDamage)
		end
	end
	if gt == gtFirePunch then
		if shoryukenDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, shoryukenDamage)
		end
	end
	if gt == gtWhip then
		if whipDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, whipDamage)
		end
	end
	if gt == gtShover then
		if batDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, batDamage)
		end
	end
	if gt == gtKamikaze then
		kamikazeGear = gear
		if kamikazeRange ~= nil then
			SetHealth(gear, kamikazeRange)
		end
		if kamikazeDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, kamikazeDamage)
		end
	end
	if gt == gtKnife then
		if cleaverDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, cleaverDamage)
		end
	end
	if gt == gtHammer then
		-- Hammer damage
		if(extraDamageUsed and hammerExtraDamage ~= nil) then
			-- Hammer with Extra Damage
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, hammerExtraDamage)
		elseif((not extraDamageUsed) and (hammerDamage ~= nil)) then
			-- Hammer without Extra Damage
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, hammerDamage)
		end
	end
	if gt == gtHammerHit and hammerStrength ~= nil then
		if hammerStrength == "inf" then
			SetTimer(gear, 0)
		else
			SetTimer(gear, hammerStrength)
		end
	end
	if gt == gtPickHammer then
		if pickHammerTimer == "inf" then
			SetTimer(gear, 0)
		elseif pickHammerTimer ~= nil then
			SetTimer(gear, pickHammerTimer)
		end
		if pickHammerDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, pickHammerDamage)
		end
		if pickHammerStraightDown == true then
			-- Disallow pickhammer to be moved left and right
			SetGearMessage(CurrentHedgehog, band(GetGearMessage(CurrentHedgehog), bnot(gmLeft+gmRight)))
			-- Input mask will be restored when pick hammer is deleted
			SetInputMask(band(GetInputMask(), bnot(gmLeft+gmRight)))
		end
		if forceField == true then
			pickHammerGear = gear
		end
	end
	if gt == gtBlowTorch then
		if blowTorchTimer == "inf" then
			SetTimer(gear, 0)
		elseif blowTorchTimer ~= nil then
			SetTimer(gear, blowTorchTimer)
		end
		if blowTorchDamage ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, blowTorchDamage)
		end
	end
	if gt == gtGrave and donorBox then
		flaggedGrave = gear
	end
	if gt == gtGirder then
		local ignore = false
		if buildLimit ~= nil and buildLimit ~= "inf" then
			if stuffBuilt >= buildLimit then
				AddCaption(loc("Building limit exceeded!"), 0xFFFFFFFF, capgrpAmmostate)
				DeleteGear(gear)
				ignore = true
			end
		end
		if not ignore then
			lastConstruction = gear
		end
	end
	if gt == gtFlake and noSnow == true then
		-- Get rid of snow flakes
		-- But only replace snow flakes, not the flakes from land spray!
		if band(GetState(gear), gsttmpFlag) == 0 then
			-- Replace snow flake gear with (harmless) visual gear flake
			local x, y = GetGearPosition(gear)
			local dx, dy = GetGearVelocity(gear)
			DeleteGear(gear)
			AddVisualGear(x, y, vgtFlake, 0, false)
		end
	end
	if gt == gtRope then
		-- Rope color and rope opacity
		if ropeColor ~= nil or ropeOpacity ~= nil then
			-- Mostly just some bitmask stuff (color is in RGBA format)
			local opacityMask
			if ropeOpacity ~= nil then
				opacityMask = bor(0xFFFFFF00, ropeOpacity)
			else
				opacityMask = 0xFFFFFFFF
			end
			local actualColor
			if ropeColor == "clan" then
				actualColor = band(GetClanColor(GetHogClan(CurrentHedgehog)), opacityMask)
			elseif ropeColor ~= nil then
				actualColor = band(ropeColor*0x100+0xFF, opacityMask)
			else
				actualColor = band(0xD8D8D8FF, opacityMask)
			end

			-- Set rope to “line” mode (needed for coloization to work)
			SetTag(gear, 1)
			-- Set rope color
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, actualColor)
		end
		ropeGear = gear
	end
	if gt == gtPortal then
		if forceField then
			portalGears[gear] = true
		end
		if portalDistance ~= nil and portalDistance ~= "inf" then
			trackGear(gear)
			setGearValue(gear, "portalDistance", portalDistance)
		end
	end
	if gt == gtTardis then
		-- Remember the Time Box' gear ID for later use, along with some additional values
		tardisGears[gear] = {}
		if tardisReturnTurns ~= nil then
			tardisGears[gear].turnsLeft = tardisReturnTurns
		end
	end
	if gt == gtSnowball then
		if strategicTools then
			-- Strategic tools: Snowball
			strategicToolsEndTurn(3000, false)
		end
		if mudballPush ~= nil then
			SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, mudballPush)
		end
	end
	if gt == gtTeleport then
		if forceField then
			teleporterGear = gear
		end
	end
	if gt == gtFlame then
		-- Remove flames
		if flameMode == "off" then
			DeleteGear(gear)
		else
			-- Make all flames sticky
			if flameMode == "sticky" then
				SetState(gear, bor(GetState(gear), gsttmpFlag))
			-- Make all flames non-sticky
			elseif flameMode == "short" then
				SetState(gear, band(GetState(gear), bnot(gsttmpFlag)))
			end
			-- Mid-air flames hurt hedgehogs
			if airFlamesHurt == true then
				SetFlightTime(gear, 0)
			end
			-- Custom flame damage
			if flameDamage ~= nil then
				SetGearValues(gear, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, flameDamage)
			end
		end
	end

	-- No jumping mode
	if noJumping and (gt == gtJetpack or gt == gtRope or gt == gtParachute) then
 		specialJumpingGear = gear
 		SetInputMask(bor(GetInputMask(), gmLJump))
 		SetInputMask(band(GetInputMask(), bnot(gmHJump)))
	end
end

function onGearResurrect(gear, vGear)
	if forceField then
		if(GetGearType(gear) == gtHedgehog) then
			if(hogsLeftSide[gear] == true and (GetX(gear) >= landCenter - centerBuffer)) then
				FindPlace(gear, false, LeftX, landCenter - centerBuffer)
				SetVisualGearValues(vGear, GetX(gear), GetY(gear))
			elseif(hogsLeftSide[gear] == false and (GetX(gear) < landCenter + centerBuffer)) then
				FindPlace(gear, false, landCenter + centerBuffer, RightX)
				SetVisualGearValues(vGear, GetX(gear), GetY(gear))
			end
		end
	end
end

function onHogRestore(gear)
	hogGears[gear] = true
end

function onGearDelete(gear)
	local gt = GetGearType(gear)

	-- Clear temp. gear variables
	if gt == gtHedgehog then
		hogGears[gear] = nil
	elseif gt == gtBirdy then
		birdyGear = nil
	elseif gt == gtRCPlane then
		rcPlaneGear = nil
	elseif gt == gtJetpack then
		saucerGear = nil
	elseif gt == gtDynamite then
		dynaGears[gear] = nil
	elseif gt == gtRope then
		ropeGear = nil
	elseif gt == gtPortal then
		portalGears[gear] = nil
	elseif gt == gtAirAttack then
		planeGears[gear] = nil
	elseif gt == gtTardis then
		tardisGears[gear] = nil
	elseif gt == gtMine then
		mineGears[gear] = nil
	elseif gt == gtPickHammer then
		pickHammerGear = nil
	elseif gt == gtTeleport then
		teleporterGear = nil
	elseif gt == gtIceGun then
		freezerGear = nil
	end

	if gt == gtHedgehog then
		if donorBox and donors[gear] then
			if flaggedGrave ~= nil then
				DeleteGear(flaggedGrave)
				flaggedGrave = nil
			end
			local x, y = GetGearPosition(gear)

			local ammos = {}
			local ammoTypes = {}

			for a=0, AmmoTypeMax do
				if a ~= amSkip and a ~= amNothing then
					local count = GetAmmoCount(gear, a)
					if count > 0 then
						ammos[a] = count
					end
				end
			end

			local box = SpawnFakeAmmoCrate(x, y, false, false)
			donorBoxes[box] = ammos
		end
		trackDeletion(gear)
	end

	if gt == gtBee and (beeTimer1 ~= nil or beeTimer2 ~= nil) then
		beeGears[gear] = nil
	end

	-- Collect donor crate and award its contents to the collector
	if donorBox and gt == gtCase and donorBoxes[gear] ~= nil then
		if band(GetGearMessage(gear), gmDestroy) ~= 0 then
			for ammoType, addCount in pairs(donorBoxes[gear]) do
				modAmmo(CurrentHedgehog, ammoType, addCount)
			end
			PlaySound(sndShotgunReload)
			AddCaption(loc("Donor box collected!"), GetClanColor(GetHogClan(CurrentHedgehog)), capgrpAmmoinfo)
			donorBoxes[gear] = nil
		end
	end

	-- Health Crate stuff
	if (teamCure or shareHealth) and gt == gtCase and band(GetGearPos(gear), 0x2) ~= 0 then
		if band(GetGearMessage(gear), gmDestroy) ~= 0 then
			-- TeamCure™
			if teamCure then
				runOnHogsInTeam(function(hog)
					SetEffect(hog, hePoisoned, 0)
				end, GetHogTeamName(CurrentHedgehog))
			end

			-- Health Socialism
			if shareHealth then
				--[[ Okay, here's how it works:
				The health pack is divided by the number of hogs in the
				team. The integer result of box health divided by hogs is then
				added to each member.
				If there is a remainder, it is split up among the hogs this
				way: We start counting from the current hedgehog, and give it
				another HP. We continue with the next hogs in team order and
				give them 1 HP each until the remainder is "used up".
				After this algorithm, the entire health pack has been used up.

				Examples:
				- 50 HP box, 5 team members: 50/10=10, so each gets 10 HP.
				- Hog 2 collects a 30 HP box, 4 team members:
				  30/4 = 7, remainder 2. So:
				  +8 HP for Hog 2 and Hog 3 (current and next), +7 HP for Hog 1 and Hog 4.
				- 1 HP, 8 team members: +1 HP only for current hog.
				]]


				-- REALLY complicated hack to find out the correct hog order
				local teamHogTable = {}
				local CurrentHedgehogID
				runOnHogsInTeam(function(hog)
					-- Skip hidden gears (will not be in hogGears table)
					if hogGears[hog] ~= nil then
						table.insert(teamHogTable, hog)
						if hog == CurrentHedgehog then
							CurrentHedgehogID = #teamHogTable
						end
					end
				end, GetHogTeamName(CurrentHedgehog))

				local newTeamHogTable = { CurrentHedgehog }
				for i=#teamHogTable, CurrentHedgehogID+1, -1 do
					table.insert(newTeamHogTable, teamHogTable[i])
				end
				for i=1, CurrentHedgehogID-1 do
					table.insert(newTeamHogTable, teamHogTable[i])
				end

				-- Now finally heal some hogs!
				-- shareHealth turned all health crates into fake ones,
				-- so we do all of the health handling manually.

				-- Heal hogs
				local boxHealth = GetHealth(gear)
				local healthPart = div(boxHealth, #teamHogTable)
				local remainder = math.fmod(boxHealth, #teamHogTable)

				for i=1, #newTeamHogTable do
					local heal
					local hog = newTeamHogTable[i]
					if i > remainder then
						heal = healthPart
					else
						heal = healthPart + 1
					end
					HealHog(hog, heal, false)
				end

				-- Sound
				PlaySound(sndShotgunReload)

				-- Custom health message
				local msg
				if(boxHealth >= 2 and #teamHogTable >= 2) then
					msg = loc("+%d (shared)")
				else
					msg = loc("+%d")
				end
				AddCaption(string.format(msg, boxHealth), GetClanColor(GetHogClan(CurrentHedgehog)), capgrpAmmoinfo)
			end
		end
	end

	-- No jumping mode
	if noJumping and (gt == gtJetpack or gt == gtRope or gt == gtParachute) then
		specialJumpingGear = nil
		SetInputMask(band(GetInputMask(), bnot(gmLJump + gmHJump)))
	end

	-- Force pick hammer to dig straight down
	if (pickHammerStraightDown or forceField) and gt == gtPickHammer then
		-- Enable left and right again for pick hammer
		SetInputMask(bor(GetInputMask(), gmLeft+gmRight))
	end

	if gt == gtKamikaze then
		kamikazeGear = nil
	end

	-- Strategic tools: Resurrection and land spray
	if strategicTools then
		if gt == gtResurrector then
			strategicToolsEndTurn(3000, false)
		elseif gt == gtLandGun then
			strategicToolsEndTurn(4000, false)
		end
	end

	-- For some reason using blow torch enables knocking again
	if noKnock and gt == gtBlowTorch then
		runOnHogs(disableKnocking)
	end
end

function onHogHide(gear)
	hogGears[gear] = nil
end

function onGameTick20()
	if portalDistance ~= nil and portalDistance ~= "inf" then
		runOnGears(function(gear)
			-- Destroy portal balls after a given distance
			local tag = GetTag(gear)
			if GetGearType(gear) == gtPortal and tag == 0 or tag == 2 then
				local col
				if tag == 0 then
					col = 0xFAB02AFF
				else
					col = 0x364DF7FF
				end
				local dist = getGearValue(gear, "portalDistance")
				dist = dist - 20
				if dist <= 0 then
					local tempE = AddVisualGear(GetX(gear)+15, GetY(gear), vgtSmoke, 0, true)
					SetVisualGearValues(tempE, nil, nil, nil, nil, nil, nil, nil, nil, nil, col)

					tempE = AddVisualGear(GetX(gear)-15, GetY(gear), vgtSmoke, 0, true)
					SetVisualGearValues(tempE, nil, nil, nil, nil, nil, nil, nil, nil, nil, col)

					tempE = AddVisualGear(GetX(gear), GetY(gear)+15, vgtSmoke, 0, true)
					SetVisualGearValues(tempE, nil, nil, nil, nil, nil, nil, nil, nil, nil, col)

					tempE = AddVisualGear(GetX(gear), GetY(gear)-15, vgtSmoke, 0, true)
					SetVisualGearValues(tempE, nil, nil, nil, nil, nil, nil, nil, nil, nil, col)

					PlaySound(sndVaporize)

					DeleteGear(gear)
				else
					setGearValue(gear, "portalDistance", dist)
				end
			end
		end)
	end
	-- Reset saucer/rc plane fuel and birdy energy if infinite
	if rcPlaneTimer == "inf" and rcPlaneGear then
		SetTimer(rcPlaneGear, 15000)
	end

	-- Reset gravity after turn freezes for 15 seconds
	if gravity ~= nil or lowGravity ~= nil then
		if wdGameTicks + 15000 < GameTime then
			SetGravity(100)
		else
			if(wdTTL ~= TurnTimeLeft) then
				wdGameTicks = GameTime
			end
		end
		wdTTL = TurnTimeLeft
	end

	-- Main bee timer
	if beeTimer2 ~= nil then
		for bee, _ in pairs(beeGears) do
			if GetGearPos(bee) == 0 then
				if GetTimer(bee) <= 20 then
					SetFlightTime(bee, GetTimer(bee))
					SetGearPos(bee, 1)
				end
			elseif GetGearPos(bee) == 1 then
				SetTimer(bee, beeTimer2)
				SetGearPos(bee, 2)
			end
		end
	end

	-- Dud mine health
	if dudMineHealth ~= nil then
		for mine, _ in pairs(mineGears) do
			if GetHealth(mine) == 0 then
				-- Dud mines explode when Damage is over 35
				SetGearValues(mine, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, 36 - dudMineHealth) -- Damage
				mineGears[mine] = nil
			end
		end
	end

	-- Epidemic
	if epidemic then
		local tdist = 22	-- Touch distance
		for hog1, t1 in pairs(hogGears) do
			if GetEffect(hog1, hePoisoned) >= 1 and GetHealth(hog1) > 0 then
				local x1, y1 = GetGearPosition(hog1)
				for hog2, t2 in pairs(hogGears) do
					local x2, y2 = GetGearPosition(hog2)
					if GetEffect(hog2, hePoisoned) == 0 and GetEffect(hog2, heInvulnerable) == 0 and GetHealth(hog2) > 0 then
						if (x2 > x1 - tdist) and (x2 < x1 + tdist) and (y2 > y1 - tdist) and (y2 < y1 + tdist) and GetHealth(hog2) > 0 then
							SetEffect(hog2, hePoisoned, GetEffect(hog1, hePoisoned))
						end
					end
				end
			end
		end
	end

	-- Custom poison damage
	if poisonDamage ~= nil then
		runOnHogs(adjustPoisonDamage)
	end

	-- Custom freeze level for hogs
	if hogFreeze ~= nil then
		runOnHogs(adjustFreezeLevel)
	end

	-- Kill poisoned
	if poisonKills and not poisonKillDone then
		if TurnTimeLeft == 0 and CurrentHedgehog ~= nil then
			runOnHogs(doPoisonKill)
			poisonKillDone = true
		end
	end

	-- Time Box timers
	if tardisReturnTurns ~= nil then
		for tardis, tardisValues in pairs(tardisGears) do
			if GetGearPos(tardis) == 4 then -- Time Box is travelling through time
				if type(tardisValues.turnsLeft) == "number" and tardisValues.turnsLeft <= 0 then
					-- Special case: The turn counter is already at 0, so we can return immediately
					SetTimer(tardis, 0)
					tardisGears[tardis].turnsLeft = nil
				elseif tardisValues.turnsLeft == "inf" then
					-- Constantly set the Time Box timer to a high value to make sure it never reaches 0
					-- so we can manually trigger the return of the Time Box
					SetTimer(tardis, 1000000)
				end
			end
		end
	end

	-- Donor boxes
	if donorBox then
		runOnHogs(checkDonors)
	end

	-- Show delayed message for modified weapon timers
	if setWeaponMessage then
		local curAmmo = GetCurAmmoType()
		if (curAmmo == amHellishBomb and hhgTimer == "manual")
		or (curAmmo == amDynamite and dynamiteTimer == "manual")
		or (curAmmo == amDrill and drillRocketTimer == "manual")
		or (curAmmo == amBallgun and ballTimer == "manual") then
			if manualTimer == nil then
				manualTimer = 5
			end
			FakeAmmoinfoCaption(curAmmo)
			setWeaponMessage = nil
		end
	end

	-- Health crate handling
	if healthCaseAmountMin ~= nil or shareHealth then
		for h=1, #healthCaseGearsTodo do
			local gear = healthCaseGearsTodo[h]
			-- Is health crate?
			if band(GetGearPos(gear), 0x2) ~= 0 then
				-- Random health case contents
				if healthCaseAmountMin ~= nil and healthCaseAmountMax ~= nil then
					SetHealth(gear, healthCaseAmountMin + GetRandom(healthCaseAmountMax-healthCaseAmountMin+1))
				end
				if shareHealth then
					-- In shareHealth mode, flag all health crates
					-- as fake crate to simplify custom health handling
					SetGearPos(gear, bor(GetGearPos(gear), 0x8))
				end
			end
		end
		healthCaseGearsTodo = {}
	end
end

function onGameTick()
	if maxPower then
		if CurrentHedgehog ~= nil then
			if band(GetGearMessage(CurrentHedgehog), gmAttack) ~= 0 then
				local at = GetCurAmmoType()
				local ok = false
				-- Check if this weapon is power-based
				for i=1,#powerWeapons do
					if at == powerWeapons[i] then
						ok = true
						break
					end
				end
				if ok then
					-- Immediately set to full launching power and force-release the attack message in the next tick
					SetGearValues(CurrentHedgehog, nil, cMaxPower - 1)
					if releaseShotInNextTick then
						SetGearMessage(CurrentHedgehog, band(GetGearMessage(CurrentHedgehog), bnot(gmAttack)))
						releaseShotInNextTick = false
					else
						releaseShotInNextTick = true
					end
				end
			end
		end
	end

	if strategicTools then
		if girderPlaced == true then
			girderPlaced = false
			strategicToolsEndTurn(4999, true)
		end
	end

	if donorBox then
		flaggedGrave = nil
	end

	-- Force field
	if forceField then
		if gameStarted and placeHogsPhase == false then
			-- Check hedgehogs
			for hog, _ in pairs(hogGears) do
				local x, y = GetGearPosition(hog)
				if(hogsLeftSide[hog] == nil) then
					-- Initialize the hogs “side”
					hogsLeftSide[hog] = x < landCenter
				else
					-- Repel hedgehogs
					local dx, dy = GetGearVelocity(hog)
					if(kamikazeGear ~= nil and hog == CurrentHedgehog) then
						-- ... except they do the kamikaze
					elseif(hogsLeftSide[hog] == true and x >= landCenter - centerBuffer) then
						SetGearPosition(hog, landCenter-centerBuffer - 1, y)
						SetGearVelocity(hog, math.min(dx, 0), dy)
					elseif(hogsLeftSide[hog] == false and x < landCenter + centerBuffer) then
						SetGearPosition(hog, landCenter + centerBuffer, y)
						SetGearVelocity(hog, math.max(dx, 0), dy)
						if dx < 0 then
							HogTurnLeft(hog, true)
						end
					end
				end
			end
			-- Check ropes
			if ropeGear ~= nil then
				-- Destroy ropes going over the center
				local x = GetX(ropeGear)
				if(hogsLeftSide[CurrentHedgehog] ~= nil and
					((hogsLeftSide[CurrentHedgehog] == true and x >= landCenter - centerBuffer) or
					(hogsLeftSide[CurrentHedgehog] == false and x < landCenter + centerBuffer)))
				then
					SetState(CurrentHedgehog, band(GetState(CurrentHedgehog), bnot(gstAttacking)))
					SetGearMessage(CurrentHedgehog, band(GetGearMessage(CurrentHedgehog), bnot(gmAttack)))
					DeleteGear(ropeGear)
				end
			end

			-- Check portals
			for portal, _ in pairs(portalGears) do
				-- Destroy all portals going over the center
				local x = GetX(portal)
				if(hogsLeftSide[CurrentHedgehog] ~= nil and
					((hogsLeftSide[CurrentHedgehog] == true and x >= landCenter - centerBuffer) or
					(hogsLeftSide[CurrentHedgehog] == false and x < landCenter + centerBuffer)))
				then
					DeleteGear(portal)
				end
			end

			-- Check pick hammer (not needed if pickhammerstraightdown=true)
			if pickHammerGear ~= nil and not pickHammerStraightDown and hogsLeftSide[CurrentHedgehog] ~= nil then
				-- Don't let pick hammer dig left/right through the center wall
				local x = GetX(pickHammerGear)
				if(hogsLeftSide[CurrentHedgehog] == true and x >= landCenter - centerBuffer - 1) then
					SetInputMask(band(GetInputMask(), bnot(gmRight)))
					SetGearMessage(CurrentHedgehog, band(GetGearMessage(CurrentHedgehog), bnot(gmRight)))
				elseif(hogsLeftSide[CurrentHedgehog] == false and x < landCenter + centerBuffer + 1) then
					SetInputMask(band(GetInputMask(), bnot(gmLeft)))
					SetGearMessage(CurrentHedgehog, band(GetGearMessage(CurrentHedgehog), bnot(gmLeft)))
				else
					-- Restore input mask when pick hammer moved away from the wall
					SetInputMask(bor(GetInputMask(), gmLeft+gmRight))
				end
			end
			-- Check teleportation
			if teleporterGear ~= nil then
				-- Let hogs only teleport within their own half
				local x, y = GetGearTarget(teleporterGear)
				local hog = CurrentHedgehog
				local unsuccess = false
				if(hogsLeftSide[hog] == true and x >= landCenter - centerBuffer) then
					SetGearTarget(teleporterGear, GetX(hog), GetY(hog))
					unsuccess = true
				elseif(hogsLeftSide[hog] == false and x < landCenter + centerBuffer) then
					SetGearTarget(teleporterGear, GetX(hog), GetY(hog))
					unsuccess = true
				end
				if unsuccess then
					AddCaption(loc("You can not go to the other side!"))
				end
				teleporterGear = nil
			end
		end

		-- Mark the map center
		if(GameTime % 8) == 0 then
			local eX, eY, tempE, color
			eX = landCenter
			eY = GetRandom(LAND_HEIGHT+1)
			tempE = AddVisualGear(eX, eY, vgtDust, 0, false)
			color = 0x808080FF
			if tempE ~= 0 then
				SetVisualGearValues(tempE, nil, nil, 0, 0, nil, nil, nil, 1, nil, color)
			end
		end
	end

	if dynamiteTimer ~= nil then
		for dynaGear, v in pairs(dynaGears) do
			if GetTimer(dynaGear) > 5000 then
				SetTag(dynaGear, 0)
			end
		end
	end

	-- Draw range circles for changed resurrector and seduction ranges
	-- TODO: Also remove the original circle as soon the engine permits it (original circles are currently (0.9.21-0.9.22) hardcoded)
	local dontDraw = false

	if band(GetState(CurrentHedgehog), gstWait+gstAttacking+gstAttacked+gstAnimation+gstHHJumping) == 0 and
	band(GetState(CurrentHedgehog), gstHHDriven) ~= 0 and
	band(GetGearMessage(CurrentHedgehog), gmLeft+gmRight) == 0 and
	ropeGear == nil then
		if resurrectorRange ~= nil and GetCurAmmoType() == amResurrector then
			if rangeCircle then
				SetVisualGearValues(rangeCircle, GetX(CurrentHedgehog), GetY(CurrentHedgehog), 20, 200, 0, 0, 100, resurrectorRange, 4, 0xFFFF80EE)
			else
				rangeCircle = AddVisualGear(GetX(CurrentHedgehog), GetY(CurrentHedgehog), vgtCircle, 0, true)
				SetVisualGearValues(rangeCircle, GetX(CurrentHedgehog), GetY(CurrentHedgehog), 20, 200, 0, 0, 100, resurrectorRange, 4, 0xFFFF80EE)
			end
		elseif seductionRange ~= nil and GetCurAmmoType() == amSeduction then
			if rangeCircle then
				SetVisualGearValues(rangeCircle, GetX(CurrentHedgehog), GetY(CurrentHedgehog), 20, 200, 0, 0, 100, seductionRange, 4, 0xFF8080EE)
			else
				rangeCircle = AddVisualGear(GetX(CurrentHedgehog), GetY(CurrentHedgehog), vgtCircle, 0, true)
				SetVisualGearValues(rangeCircle, GetX(CurrentHedgehog), GetY(CurrentHedgehog), 20, 200, 0, 0, 100, seductionRange, 4, 0xFF8080EE)
			end
		else
			dontDraw = true
		end
	else
		dontDraw = true
	end
	if dontDraw then
		if rangeCircle ~= nil then
			DeleteVisualGear(rangeCircle)
		end
		rangeCircle = nil
	end
end

function onGameStart()
	-- Place random girders
	if girders ~= nil then
		local points = {}
		-- Will be set to true if we have exceeded the tryLimit once
		local dontBotherTrying = false
		local buffer = 160
		local tryLimit = 500
		--[[ The complexitiy of this code is O(n^2) but the algorithm may terminate early
		if it fails to validly place a girder ]]
		for i=1,girders do
			local x, y
			-- Try a couple of times to place a girder without being too close to other placed girders.
			local tries = 0
			while tries < tryLimit do
				-- Get a random girder position
				tries = tries + 1
				x, y = GetRandom(LAND_WIDTH-buffer)+buffer, GetRandom(LAND_HEIGHT-buffer)+buffer
				if dontBotherTrying then
					--[[ To reduce the complexity, we don't go through the next loop if we have once
					exceeded the tryLimit ]]
					break
				end
				local placementOkay = true
				-- Check collisions with gears and land
				--[[ NOTE: Remove this line to make girders appear anywhere, even in land and near gears. In combination with
				EraseSprite, this can be used to create nice “girder holes”. The HWP file currently contains a legacy sprite
				for erasing (custom sprite 1).
				This MAY be a nice candidate for another parameter but it might be a bit glitchy ]]
				placementOkay = not TestRectForObstacle(x-div(buffer,2), y-div(buffer,2), x+div(buffer,2), y+div(buffer,2), false)
				-- Check whether this position it is too close to other girders
				if placementOkay then
					for p=1,#points do
						if math.sqrt(math.pow(math.abs(points[p].x - x), 2) + math.pow(math.abs(points[p].y -y), 2)) < buffer then
							placementOkay = false
							break
						end
					end
				end
				if placementOkay then
					break
				end
			end
			if tries >= tryLimit then
				--[[ If we have once tried too many times, we stop adding girders unless
				forceLimits is false. DontBotherTrying is used to kinda reduce the algorithmic
				complexity for the following iterations if forceLimits is false ]]
				dontBotherTrying = true
				if forceLimits then
					break
				end
			end
			table.insert(points, {x=x, y=y})
		end
	
		-- Place girders
		for i=1,#points do
			PlaceSprite(points[i].x, points[i].y, sprAmGirder, 0)
		end
	end

	-- David and Goliath
	if davidAndGoliath then
		for teamName, teamMembers in pairs(teams) do
			hogCounter = 0
			local goliath
			local healthForGoliath = 0
			runOnHogsInTeam(function(hog)
				-- Goliath
				if hogCounter == 0 then
					goliath = hog
				-- David
				else
					--[[ Remove half of the health (rounded down) ]]
					local removedHealth = div(GetHealth(hog), 2)
					healthForGoliath = healthForGoliath + removedHealth
					SetHealth(hog, GetHealth(hog) - removedHealth)
				end
				hogCounter = hogCounter + 1
			end, teamName)

			-- Add all health to Goliath
			SetHealth(goliath, GetHealth(goliath) + healthForGoliath)
		end
	end

	-- Prepare no invasion mode
	if forceField then
		landCenter = div(LAND_WIDTH, 2)
	end
end

function onHogAttack(ammoType)
	if ammoType == amExtraTime then
		local n = 30
		if extraTime ~= nil then
			n = extraTime
			SetTurnTimeLeft(math.min(MAX_TURN_TIME, TurnTimeLeft - 30000 + extraTime))
		end
	elseif ammoType == amExtraDamage then
		extraDamageUsed = true
	elseif ammoType == amLowGravity then
		if gravity ~= nil or lowGravity ~= nil then
			local base = 100
			if gravity then
				base = gravity
			end
			if lowGravity == nil then
				lowGravity = 50
			end
			SetGravity(div(base * lowGravity, 100))
		end
	end
end

function FakeAmmoinfoCaption(ammoType)
	local ammoCount = GetAmmoCount(CurrentHedgehog, ammoType)
	local str

	local infStr, finStr

	if ammoType == amHellishBomb or ammoType == amDynamite or ammoType == amDrill or ammoType == amBallgun then
		finStr = loc("%s (%d), %d sec")
		infStr = loc("%s, %d sec")
	end

	if ammoCount == 100 then
		str = string.format(GetAmmoName(ammoType), infStr, manualTimer)
	else
		str = string.format(GetAmmoName(ammoType), finStr, GetAmmoCount(CurrentHedgehog, ammoType), manualTimer)
	end
	AddCaption(str, GetClanColor(GetHogClan(CurrentHedgehog)), capgrpAmmoinfo)
end

function onTimer(timer)
	manualTimer = timer
	local curAmmo = GetCurAmmoType()
	if CurrentHedgehog ~= nil then
		if (curAmmo == amHellishBomb and hhgTimer == "manual")
		or (curAmmo == amDynamite and dynamiteTimer == "manual")
		or (curAmmo == amDrill and drillRocketTimer == "manual")
		or (curAmmo == amBallgun and ballTimer == "manual") then
			FakeAmmoinfoCaption(curAmmo)
		end
	end
end

function onSetWeapon()
	setWeaponMessage = true
end
onSlot = onSetWeapon

function onAttack()
	if birdyGear ~= nil then
		if birdyEggs == "inf" then
			-- We kinda “fake” infinity by making sure we never run out of eggs
			SetFlightTime(birdyGear, 9001)
		end
	end
	if rcPlaneGear ~= nil then
		if rcPlaneBombs == "inf" then
			-- We kinda “fake” infinity by making sure we never run out of bombs
			SetHealth(rcPlaneGear, 9001)
		end
	end
	if kamikazeGear ~= nil then
		if kamikazeTrigger then
			SetHealth(kamikazeGear, 0)
		end
	end
end

function suddenDeathSpecials()
	if sdState ~= 1 then
		-- Emulate Sudden Death effect if it is disabled by the game scheme
		if HealthDecrease == 0 and WaterRise == 0 and (sdPoison or sdOneHP ) then
			AddCaption(loc("Sudden Death!"), capcolDefault, capgrpGameState)
			PlaySound(sndSuddenDeath)
		end
		if sdPoison then
			runOnHogs(function(hog)
				if poisonDamage ~= nil then
					SetEffect(hog, hePoisoned, poisonDamage)
				else
					SetEffect(hog, hePoisoned, 5)
				end
			end)
		end
		if sdOneHP then
			runOnHogs(function(hog)
				SetHealth(hog, 1)
			end)
		end
		sdState = 1
	end
end

onSuddenDeath = suddenDeathSpecials

function onNewTurn()
	gameStarted = true
	releaseShotInNextTick = false
	manualTimer = nil
	extraDamageUsed = false
	turnsCounter = turnsCounter + 1

	if(placeHogsPhase) then
		if turnsCounter > #hogGears then
			placeHogsPhase = false
		end
	end

	if buildLimit ~= nil and buildLimit ~= "inf" then
		stuffBuilt = 0
		if buildRange == "inf" then
			SetMaxBuildDistance(0)
		elseif buildRange ~= nil then
			SetMaxBuildDistance(buildRange)
		else
			SetMaxBuildDistance()
		end
	end

	-- Set gravity
	if gravity ~= nil then
		wdGameTicks = GameTime
		SetGravity(gravity)
	end

	-- No jumping mode
	if noJumping then
		 SetInputMask(band(GetInputMask(), bnot(gmLJump + gmHJump)))
	end

	-- Disable knocking
	if noKnock then
		runOnHogs(disableKnocking)
	end

	-- Set gravity
	if gravity ~= nil then
		SetGravity(gravity)
	end

	-- Adjust wind
	if fairWind or maxWind ~= nil then
		local newWind
		if maxWind ~= nil then
			if maxWind == 0 then
				newWind = 0
			else
				newWind = GetRandom(maxWind*2+1) - maxWind
			end
		else
			newWind = GetRandom(201)-100
		end

		if fairWind then
			if lastRound == nil or lastRound ~= TotalRounds then
				SetWind(newWind)
			end
		elseif maxWind ~= nil then
			SetWind(newWind)
		end
	end

	if TotalRounds == SuddenDeathTurns + 1 then
		suddenDeathSpecials()
	end

	-- Reset “poison kills” modifier for this turn
	if poisonKills then
		poisonKillDone = false
	end

	-- Force-return Tim eBoxes when using turn-based return setting
	if tardisReturnTurns ~= nil then
		for tardis, tardisValues in pairs(tardisGears) do
			if GetGearPos(tardis) == 4 and type(tardisValues.turnsLeft) == "number" then
				tardisGears[tardis].turnsLeft = tardisGears[tardis].turnsLeft - 1
				if tardisGears[tardis].turnsLeft <= 0 then
					SetTimer(tardis, 0)
					tardisGears[tardis].turnsLeft = nil
				end
			end
		end
	end

	-- Count rounds
	lastRound = TotalRounds
end

function onGirderPlacement(frameIdx, x, y)
	if(buildLimit ~= nil and buildLimit ~= "inf") then
		stuffBuilt = stuffBuilt + 1
		AddCaption(string.format(loc("Buildings left: %d"), buildLimit - stuffBuilt), 0xFFFFFFFF, capgrpAmmostate)
		if stuffBuilt >= buildLimit then
			SetMaxBuildDistance(1)
		end
	end
	if strategicTools then
		strategicToolsHandlePlacement()
	end
end

onRubberPlacement = onGirderPlacement

--[[ Helper functions ]]
function hedgepot()
	local default_percent_min = 33
	local default_percent_max = 300
	local mutators = {
		{ type = "GameFlag", name = "gfLaserSight" },
		{ type = "GameFlag", name = "gfInvulnerable" },
		{ type = "GameFlag", name = "gfResetHealth" },
		{ type = "GameFlag", name = "gfVampiric" },
		{ type = "GameFlag", name = "gfKarma" },
		{ type = "GameFlag", name = "gfSharedAmmo" },
		{ type = "GameFlag", name = "gfResetWeps" },
		{ type = "GameFlag", name = "gfPerHogAmmo" },
		{ type = "GameFlag", name = "gfBottomBorder" },
		{ type = "GameFlag", name = "gfSwitchHog" },
		{ type = "CoreNumber", name = "HealthCaseProb", absolute_min = 0, absolute_max = 100 },
		{ type = "CoreNumber", name = "DamagePercent" },
		{ type = "CoreNumber", name = "MinesNum" },
		{ type = "CoreNumber", name = "MineDudPercent", absoulte_min = 0, absolute_max = 100 },
		{ type = "CoreNumber", name = "Explosives" },
		{ type = "GameFlag", name = "gfKarma" },
		{ type = "GameFlag", name = "gfMoreWind" },
		{ type = "truth", name = "airFlamesHurt" },
		{ type = "truth", name = "poison" },
		{ type = "truth", name = "noJump" },
		{ type = "truth", name = "noKnock" },
		{ type = "truth", name = "teamCure" },
		{ type = "truth", name = "shareHealth" },
		{ type = "truth", name = "davidAndGoliath" },
		{ type = "truth", name = "strategicTools" },
		{ type = "truth", name = "sdPoison" },
		{ type = "truth", name = "sdOneHP" },
		{ type = "truth", name = "poisonKills" },
		{ type = "truth", name = "epidemic" },
		{ type = "truth", name = "maxPower" },
		{ type = "truth", name = "fairWind" },
		{ type = "enum", name = "flameMode", enum = { "normal", "sticky", "short", "off" } },
		{ type = "num", name = "maxWind", absolute_min = 0, absolute_max = 100, default = 100 },
		{ type = "num", name = "gravity", absolute_min = 10, absolute_max = 200, default = 100 },
		{ type = "num", name = "poisonDamage", absolute_min = 1, absolute_max = 20, default = 5 },
		{ type = "num", name = "hogFriction", absolute_min = -10, absolute_max = 10, default = 0 },
		{ type = "num", name = "buildLimit", absolute_min = 1, absolute_max = 5, default = 2 },
	}
	local util_weapons_modifiers =
	{
		{ type = "truth", name = "kamikazeTrigger" },
		{ type = "truth", name = "pickHammerStraightDown", text=loc("Pick hammer (no skewing)") },
		{ type = "enum", name = "MinesTime", enum = { -1000, 0, 1000, 2000, 3000, 4000, 5000 } },
		{ type = "enum", name = "tardisReturnTurns", enum = { 0, 1, 3, 5, "inf", } },
		{ type = "set", name = "saucerFuel", value = "inf" },
		{ type = "set", name = "birdyEnergy", value = "inf" },
		{ type = "set", name = "birdyEggs", value = "inf", text=loc("Birdy eggs (infinite)") },
		{ type = "set", name = "rcPlaneBombs", value = "inf", text=loc("RC Plane bombs (infinite)") },
		{ type = "set", name = "hhgTimer", value = "manual", text=loc("Hellish hand-grenade timer (manual)")},
		{ type = "set", name = "ballTimer", value = "manual", text=loc("Ball timer (manual)")},
		{ type = "set", name = "buildRange", value = "inf" },
		{ type = "num", name = "buildRange", default = 256, text=loc("Construction/Rubber range (%d%%)"), numtype="perc" },
		{ type = "num", name = "saucerFuel", default = 2000 },
		{ type = "num", name = "birdyEnergy", default = 2000 },
		{ type = "num", name = "rcPlaneTimer", default = 15000},
		{ type = "num", name = "freezerFuel", default = 1000, text=loc("Freezer fuel (%d%%)"), numtype="perc"},
		{ type = "num", name = "flamethrowerFuel", default = 500, text=loc("Flamethrower fuel (%d%%)"), numtype="perc"},
		{ type = "num", name = "cakeTimer", default = 2048, text=loc("Cake walking time (%d%%)"), numtype="perc"},
		{ type = "num", name = "kamikazeRange", default = 2048, text=loc("Kamikaze range (%d%%)"), numtype="perc"},
		{ type = "num", name = "ballTimer", default = 5000, text=loc("Ball timer (%.1fs)"),numtype="time"},
		{ type = "num", name = "pickHammerTimer", default = 4000, text=loc("Pickhammer time (%.1fs)"), numtype="time"},
		{ type = "num", name = "blowTorchTimer", default = 7500, text=loc("Blowtorch time (%.1fs)"), numtype="time"},
		{ type = "num", name = "birdyEggs", default = 2, text=loc("Birdy eggs (%d)"), numtype="abs"},
		{ type = "num", name = "rcPlaneBombs", default = 3, text=loc("RC Plane bombs (%d)"), numtype="abs"},
		{ type = "num", name = "ballGunBalls", default = 51, text=loc("Number of ball gun balls (%d)"), numtype="abs"},
		{ type = "num", name = "pianoBounces", default = 5, text=loc("Piano terrain bounces (%d)"), numtype="abs"},
		{ type = "num", name = "airAttackBombs", default = 6, text=loc("Air attack bombs (%d)"), numtype="abs"},
		{ type = "num", name = "mineStrikeMines", default = 6, text=loc("Mine strike mines (%d)"), numtype="abs"},
		{ type = "num", name = "drillStrikeDrills", default = 6, text=loc("Drill strike drills (%d)"), numtype="abs"},
		{ type = "num", name = "deagleStrength", default = 50, text=loc("Desert Eagle digging strength (%d%%)"), numtype="perc"},
		{ type = "num", name = "sniperStrength", default = 50, text=loc("Sniper rifle digging strength (%d%%)"), numtype="perc"},
		{ type = "num", name = "hammerStrength", default = 125, text=loc("Hammer digging strength (%d%%)"), numtype="perc"},
		{ type = "num", name = "extraTime", default = 30000, absolute_min = 15000, absolute_max = 45000 },
		{ type = "num", name = "lowGravity", default = 50},
		{ type = "num", name = "drillRocketTimer", default = 5000, text=loc("Drill rocket timer (%.1fs)"), numtype="time"},
		{ type = "num", name = "hhgTimer", default = 5000, text=loc("Hellish hand-grenade timer (%.1fs)"), numtype="time"},
	}
	local conflicts = {
		sdPoison = { "sdOneHP" },
		sdOneHP = { "sdPoison" },
		gfInvulnerable = { "gfVampiric", "gfKarma", "teamCure", "poison", "sdPoison", "shareHealth", "gfResetHealth" },
		gfVampiric = { "gfInvulnerable" },
		gfKarma = { "gfInvulnerable" },
		teamCure = { "gfInvulnerable" },
		poison = { "gfInvulnerable" },
		sdPoison = { "gfInvulnerable" },
		shareHealth = { "gfInvulnverable" },
		gfResetHealth = { "gfInulnverable" },
		gfArtillery = { "noJump" },
		noJump = { "gfArtillery " },
		gfSharedAmmo = { "gfPerHogAmmo" },
		gfPerHogAmmo = { "gfSharedAmmo" },
	}

	local text_counter = 0
	local texts = {}

	local function roll(reel)
		local pickID = GetRandom(#reel)+1
		local pick = reel[pickID]
		if pick.type == "GameFlag" then
			if GetGameFlag(_G[pick.name]) == true then
				DisableGameFlags(_G[pick.name])
			else
				EnableGameFlags(_G[pick.name])
			end
		elseif pick.type == "CoreNumber" then
			if pick.absolute_min ~= nil and pick.absolute_max ~= nil then
				_G[pick.name] = GetRandom(pick.absolute_max - pick.absolute_min + 1) + pick.absolute_min
			else
				_G[pick.name] = div((GetRandom(default_percent_max - default_percent_min) + default_percent_min) * _G[pick.name], 100)
			end
		elseif pick.type == "enum" then
			local e = GetRandom(#pick.enum - 1)+1
			_G[pick.name] = pick.enum[e]
		elseif pick.type == "truth" then
			_G[pick.name] = true
		elseif pick.type == "num" then
			if pick.absolute_min and pick.absolute_max then
				_G[pick.name] = GetRandom(pick.absolute_max - pick.absolute_min + 1) + pick.absolute_min
			else
				_G[pick.name] = div((GetRandom(default_percent_max - default_percent_min) + default_percent_min) * pick.default, 100)
			end
		elseif pick.type == "set" then
				_G[pick.name] = pick.value
		end
		if pick.text ~= nil then
			text_counter = text_counter + 1
			if pick.type == "num" then
				local arg
				if pick.numtype == "abs" then
					arg = round(_G[pick.name])
				elseif pick.numtype == "time" then
					arg = _G[pick.name]/1000
				else
					arg = round(_G[pick.name]/pick.default*100)
				end
				table.insert(texts, string.format(pick.text, arg))
			elseif pick.type == "set" then
				table.insert(texts, string.format(pick.text))
			end
		end
		table.remove(reel, pickID)
	end

	-- “Roll” the “reels”
	local rolls_1 = GetRandom(5)+1
	local weapons_mod = GetRandom(100)
	local rolls_2 = 0

	for i=1,rolls_1 do
		roll(mutators)
	end
	if weapons_mod < 20 then
		roll(util_weapons_modifiers)
	end

	return text_counter, texts
end

-- Functions for strategic tools
function strategicToolsEndTurn(baseRetreatTime, girderUsed)
	SetState(CurrentHedgehog, bor(GetState(CurrentHedgehog), gstAttacked))
	local retreatTime
	retreatTime = div(baseRetreatTime * GetAwayTime, 100)
	-- Unselect girder/rubber so the hog can walk again
	if girderUsed then
		SetWeapon(amNothing)
	end
	Retreat(retreatTime)
end

function strategicToolsHandlePlacement()
	girderPlaced = true
	strategicToolsEndTurn(5000, true)
end

-- Handle modified poison damage
function adjustPoisonDamage(hog)
	if GetEffect(hog, hePoisoned) > 0 then
		if GetEffect(hog, hePoisoned) ~= poisonDamage then
			SetEffect(hog, hePoisoned, poisonDamage)
		end
	end
end

-- Handle modified freeze level for freshly frozen hogs
function adjustFreezeLevel(hog)
	if GetEffect(hog, heFrozen) == 199999 and freezerGear == nil then
		if getGearValue(hog, "freezeLevelAdjusted") ~= true then
			SetEffect(hog, heFrozen, hogFreeze)
			setGearValue(hog, "freezeLevelAdjusted", true)
		end
	else
		setGearValue(hog, "freezeLevelAdjusted", false)
	end
end

-- Kill poisoned hogs with low health
function doPoisonKill(hog)
	local poison = GetEffect(hog, hePoisoned)
	local health = GetHealth(hog)
	if poison > 0 then
		if health - poison <= 0 then
			SetHealth(hog, 0)
		end
	end
end

-- Hogs of Steel mode
function disableKnocking(gear)
	SetState(gear, bor(GetState(gear), gstNotKickable))
end

-- Drop donor box if it was the last hog or the king
function checkDonors(hog)
	if band(GetState(hog), gstHHDeath) ~= 0 and band(GetState(hog), gstDrowning) == 0 then
		if GetGameFlag(gfKing) and hogNumbers[GetHogTeamName(hog)][1] == hog then
			donors[hog] = true
		else
			hogCounter = 0
			runOnHogsInTeam(countHog, GetHogTeamName(hog))
			if hogCounter == 1 then
				donors[hog] = true
			end
		end
	end
end

-- Misc.
-- Used for tracking functions to count hogs in a team etc.
function countHog(gear)
	hogCounter = hogCounter + 1
end

function round(num)
	if num >= 0 then
		return math.floor(num + 0.5)
	else
		return math.ceil(num - 0.5)
	end
end

-- Given a time in milliseconds, converts the number into seconds and rounds it up
--[[ Examples:
	0 → 0
	2000 → 2
	500 → 1
	999 → 1
	1500 → 2
	2456 → 3
]]
function timeToNextSec(milliSecs)
	return div(milliSecs + 999, 1000)
end

-- Truncate string input number (safe math.floor(tonumber(x)) alternative)
function truncStrNum(strNum)
	if strNum == nil then return nil end
	local s = string.match(strNum, "(%d*)")
	if s ~= nil then
		return tonumber(s)
	else
		return nil
	end
end

-- Changes the ammo count of ammoType of the hog
-- It adds addCount to the hog
-- If addCount is 100, the hog gets infinite ammo
-- For finite numbers, the hog can never end up with more than 99 of the same ammo
function modAmmo(hog, ammoType, addCount)
	local currentCount = GetAmmoCount(hog, ammoType)
	if showMessage == nil then showMessage = true end
	if currentCount ~= 100 and addCount ~= 100 then
		local newCount
		if addCount == 100 then
			newCount = 100
		else
			newCount = math.max(0, math.min(99, currentCount + addCount))
		end
		AddAmmo(CurrentHedgehog, ammoType, newCount)
	end
end
