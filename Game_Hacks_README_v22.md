# Game Hacks

## Introduction
Game Hacks is a Hedgewars game style which allows you to manipulate over 100 different aspects of Hedgewars using the script parameter in the game scheme. Create the craziest games you can ever imagine.

Game Hacks is supported for Hedgewars version 1.0.0 and later.

This document explains how Game Hacks works.

## Description
Game Hacks adds additional game modifiers and settings in the following categories:

- Environment
- Information
- Hedgehogs
- Turn-based play
- Sudden Death events
- Weapon parameters (per-weapon damage, timer, etc.) 
- Utility parameters
- Eye-candy/gimmicks

The most important enabled modifiers will be displayed and explained briefly in the mission panel (shown at the start of the game or when you hit the mission panel key (default: [M]). As a rule of thumb, modifiers which are not obvious and have a big impact on gameplay are mentioned. Modifiers which are either obvious or only have little or no impact are usually not mentioned. Also, most modified weapons and utility effects are usually not mentioned, but their weapon description might be modified.

One important rule: If there is a conflict between one of Hedgewars' built-in settings and a script setting, the script setting takes precedence.

With an empty script parameter, there's only an cosmetic change: Generic ammo symbols for Extra Time and Extra Damage.

## Basic syntax
This script can be configured mostly with the script parameters.

The script parameters are specified in a `key=value` format, and each pair is delimeted by a comma. Each value has one of multiple parameter types, which are described below. Each key can be specified in long form or in short form. The long form is descriptive. The short form is only 1 or 2 characters long.

Note that the entire script parameter field should not become longer than 245 characters, longer input can lead to problems in network games. This is a problem of Hedgewars. If you are using a lot of keys, you can use the short form. However, even with the short form it is possible to exhaust the character limit if you use a lot of parameters, so keep this in mind.

Try to use the long form when possible because it's easier to remember.

All values are optional.

See also the “Examples” section below.

### Parameter types (see list of keys)
* num
	* A whole number.
	* Examples: `534`, `54`, `-13`
* hex
	* A whole non-negative number in hexadecimal notation. Valid digits are 0-9 and A-F or a-f.
	* Examples: `E`, `FF`, `84AE`, `a0a0a0`
* time
	* Time. Times are interpreted as milliseconds by default, but you can append “`ms`” or “`s`” for milliseconds or seconds, respectively. Seconds can be specified as decimal numbers with “`.`” as the decimal separator and with up to 3 decimal places, but milliseconds must always be whole numbers.
	* Examples (all mean 2.5 seconds): `2500`, `2500ms`, `2.5s`
* %
	* A percentage or a plain number. If you append a percent sign to the number, it is interpreted as percentage from the default number. Only use whole numbers for percentages. Note that specifying the plain number may be much more precise, but for most of the time, specifying the percentage might be precise enough for you.
	* Examples: `2000`, `100%`, `125%`
* truth
	* Used for on/off states. Can be either “`true`” (feature is enabled) or “`false`” (feature is disabled). You can also use “`t`” or “`f`” as abbreviations.
* choice
	* You can choose one of multiple values which is written in the description.

In the table below, some types will have an asterisk (“\*”). This means that the special value “`inf`” (or short “`i`”) is allowed here, meaning an infinite amount or time.

Some types will have a dash (“-”). This means that you can also specify a range of values by writing two values of this type separated by a dash (e.g. “`4-52`”). The first value must be smaller or equal than the second value, otherwise they will be ignored.

Some parameters permit special values, see the description.

Most parameters out of reasonable bounds (i.e. a barrel health of 0 or negative gravity) will be capped for technical reasons to avoid a buggy or unexpected behaviour. This is a safety feature but it can be disabled if you set `forcelimits` to `false`. **Warning**: If you use `forcelimits`, you're on your own; expect broken behaviour.

Two special values are available for the types “num”, “time” and “%”:

* `min` for the smallest possible (or technically feasible) value and
* `max` for the biggest one (including `inf`).

The boundaries are the same as for `forcelimits`.

## List of keys

These are all the keys that Game Hacks supports:

    Key                     Short   Type    Default Description
    -----------------------+-------+-------+-------+-------------------------------------------------------------------------
    <<< ENVIRONMENT >>>
    barrelhealth            b       num-    60      Initial health of barrels.
    healthcratehealth       eh      num-    N/A     Health in health crates will be randomly set within this range
    sturdycrates            ec      truth   false   Crates can't be destroyed.
    maxcratedrops           eC      num     5       Max. crates that can be in game before random crate dropping stops
    maxwind                 w       %       100     Limit the maximum wind strength (0-100).
    fairwind                W       truth   false   Wind stays the same within the same round.
    gravity                 g       %       100     Base gravity (without utilities) in percent.
    flamemode               ef      choice  normal  “off”: Disable flames and flame-based weapons.
                                                    “sticky”: All flames keep burning through turns.
                                                    “short”: No flames keep burning through turns.
                                                    “normal”: No change.
    airflameshurt           a       truth   false   Flames in mid-air can hurt hedgehogs.
    nosnow                  Q       truth   false   Snow does not pile up in themes where it normally snows (i.e. Snow, Christmas)
    girders                 G       num     0       Number of randomly placed girders, not all might be placed if too many. EXPERIMENTAL!
    forcefield              ei      truth   false   Hedgehogs can not enter the opposite side of the terrain. EXPERIMENTAL!
    
    <<< INFORMATION >>>
    showminetimer           im      truth   false   Make it possible to see timer of mines and air mines with object info key if mines time is random.
    
    <<< HEDGEHOGS >>>
    nojump                  j       truth   false   Hogs can't jump.
    noknock                 k       truth   false   “Hogs of Steel”: Hogs can't be knocked away by sliding hogs.
    hogfriction             0       num     0       Difference from default hog friction. Sane values are from -10 to 10.
    poisondamage            P       num     5       Damage for poisoned hogs per round.
    poisonkills             n       truth   false   Poison reduces health down to 0 instead of 1.
    poison                  p       truth   false   All hedgehogs start poisoned.
    hogfreeze               hf      %       199999  Initial “freeze level” when freezing hogs with freezer. See parameter notes for details.
    epidemic                he      truth   false   Poisoned hedgehogs infect other hedgehogs they touch.
    teamcure                t       truth   false   Collecting a health crate will remove poison from all hogs in team.
    sharehealth             h       truth   false   Health of crates is shared among team mates.
    davidandgoliath         d       truth   false   The 1st hog in team removes half the health of its team mates and adds it to its own.
    donorbox                q       truth   false   The last dying hog (or the king in King Mode) of a team leaves a box with all the hog weapons inside.
    maxpower                hm      truth   false   Thrown/launched weapons will be thrown/launched at full power (like mortar).
    
    <<< TURNS >>>
    ready                   r       time    5s      Ready time before the turn starts.
    strategictools          z       truth   false   Constructions, rubbers, land sprays, mud balls and resurrections end the turn.
    
    <<< SUDDEN DEATH >>>
    sdpoison                S       truth   false   Poison all hogs in Sudden Death (once).
    sd1hp                   1       truth   false   Set the health of all hogs to 1 HP in Sudden Death (once).
    
    <<< FUEL / DURATION >>>
    saucerfuel              f       %*      2000    Fuel of a flying saucer.
    birdyenergy             B       %*      2000    Flapping energy of Birdy.
    landsprayfuel           L       %       1000    Fuel of land spray.
    freezerfuel             u       %       1000    Fuel of freezer.
    flamethrowerfuel        F       %       500     Fuel of flamethrower.
    rcplanetimer            C       time*   15s     Time of RC plane before fuel is gone.
    minetimer               8       time-   3s      Timer of ALL ordinary mines, with milliseconds precision. Use range for a random timer within the range.
    stickyminetimer         s       time-   500ms   Timer of sticky mines. Use range for a random timer within the range.
    airminetimer            Y       time-   750ms   Custom timer of air mines. Use range for a random timer within the range.
    hellishtimer            6       time    5s      Timer of hellish hand-grenade. Use “manual” or “m” for allowing to set timer by user.
    dynamitetimer           5       time    5s      Timer of dynamite. Use “manual” or “m” for allowing to set timer by user.
    caketimer               c       %       2048    Walking time of cake (percentage).
    beedelay                x       time    500ms   Delay of homing bee before it starts homing.
    beetimer                y       time    5s      Max. flight time of homing bee after it started homing
    drillrockettimer        3       time    5s      Detonation timer of drill rocket (not from plane). Use “manual” or “m” for allowing to set timer by user.
    napalmbombtimer         tN      time    1s      Detonation timer of a napalm bomb.
    kamikazerange           K       %       2048    Range of kamikaze.
    balltimer               m       time    5s      Explosion timer of ball from ball gun. Use “manual” or “m” for allowing to manually set timer with the ball gun.
    pickhammertimer         I       time*   4s      Active time of pick hammer.
    blowtorchtimer          T       time*   7.5s    Active time of blowtorch.
    poisoncloudtimer        tp      time    5s      Life timer of a poison cloud from old limburger. 5 seconds max.
    portaldistance          tP      num*    inf     How far portal balls can fly. “inf” = no limit.

    <<< WEAPON EFFECTS >>>
    birdyeggs               E       num*    2       Number of eggs for Birdy
    rcplanebombs            R       num*    3       Number of bombs of RC plane.
    ballgunballs            U       num     51      Number of balls from ballgun.
    pianobounces            O       num     5       Number of terrain bounces of piano before it falls into water.
    airattackbombs          A       num     6       Bombs in an air attack.
    minestrikemines         M       num     6       Mines in a mine strike.
    drillstrikedrills       D       num     6       Drills in a drill strike.
    napalmbombs             N       num     6       Bombs in a napalm attack.
    planedrops              4       num     6       Shorthand: Set all above 4 parameters at once.
    airattackgap            wA      %       30      Distance between 2 missile drops in air attack.
    minestrikegap           wM      %       30      Distance between 2 mine drops in mine strike.
    drillstrikegap          wD      %       30      Distance between 2 drill drops in drill strike.
    napalmgap               wN      %       30      Distance between 2 napalm bomb drops in napalm attack.
    planedropgap            2       %       30      Shorthand: Set all above 4 parameters at once.
    dudminehealth           wu      %       36      How much “health” dud mines have before they explode.
    airmineseekrange        wr      %*      175     Distance (in pixels) at which air mines start seeking.
    airmineseekspeed        we      %       536871  Speed at which air mines seek hogs.
    airminefriction         wf      %       1610613 Air friction of air mine.
    hammerstrength          wh      %*      125     How deep a (successful) hammer hit digs into land.
    kamikazetrigger         wk      truth   false   If true, kamikaze can be detonated early with the attack key.
    deaglestrength          wd      %       50      How deep a desert eagle shot digs into land.
    sniperstrength          ws      %       50      How deep a sniper rifle shot digs into land.
    sinestrength            wS      num     1       Thickness of sine gun shot.
    mudballstrength         wm      %       200000  Pushing strength of a mud ball.
    seductionrange          7       %       250     Range of seduction in pixels. EXPERIMENTAL!
    
    <<< DAMAGE AND EXPLOSION SIZE >>>
    
    airminedamage           da      %       30      Explosion damage and range of air mine.
    minedamage              d8      %       50      Mine damage.
    stickyminedamage        dt      %       30      Sticky mine damage.
    grenadedamage           dg      %       50      Grenade damage.
    hellishdamage           dH      %       90      Hellish hand-grenade damage.
    clusterbombdamage       dl      %       20      Cluster bomb damage (main bomb).
    clusterdamage           dL      %       25      Damage of a cluster (from cluster bomb or mortar).
    melondamage             dm      %       75      Melon bomb damage (main piece).
    melonpiecedamage        dM      %       75      Melon bomb piece damage.
    balldamage              dU      %       40      Damage of a ball from the ball gun.
    bazookadamage           db      %       50      Bazooka shell damage.
    mortardamage            do      %       20      Mortar damage (main projectile).
    beedamage               dx      %       50      Bee damage.
    dynamitedamage          d5      %       75      Dynamite damage.
    cakedamage              dc      %       75      Cake damage.
    batdamage               dB      %       30      Baseball bat damage.
    shoryukendamage         dF      %       30      Shoryuken damage.
    whipdamage              dw      %       30      Whip damage.
    rcplanedamage           dr      %       25      RC plane damage.
    cleaverdamage           dv      %       40000   Scaling value of the impact damage of a cleaver. The damage does not linearly increase.
    eggdamage               dE      %       10      Damage of an egg (from Birdy).
    pianodamage             dp      %       80      Damage of a single Piano explosion (note: each impact creates 3 explosions).
    limburgerdamage         di      %       20      Old limburger damage.
    deagledamage            dd      %       7       Damage of a single Desert Eagle shot.
    minigundamage           dG      %       2       Damage of a single minigun bullet.
    shotgundamage           dO      %       25      Damage of a single shotgun shot.
    sniperdamage            ds      %       100000  Scaling value of the sniper rifle's damage.
    sinedamage              dS      %       35      Damage of a sine wave from the sine gun.
    kamikazedamage          dK      %       30      Damage of a kamikaze impact and its explosion.
    airbombdamage           dA      %       30      Damage of a bomb from an air strike or en RC plane.
    drillrocketdamage       d3      %       50      Damage of a launched drill rocket.
    drillstrikedrilldamage  dD      %       30      Damage of a drill rocket from the drill strike.
    blowtorchdamage         dT      %       2       Damage of a single hit with a blowtorch.
    pickhammerdamage        dI      %       6       Damage of a single hit with a pickhammer.
    flamedamage             df      %       2       Damage of a small flame (larger flames deal 3 or 4 times this damage)
    hogdamage               dh      %       30      Damage of the explosion of a dying hedgehog.
    cratedamage             dC      %       25      Damage of an exploding crate.
    barreldamage            de      %       75      Damage of an exploding barrel.
    hammerdamage            dj      num     3       Denominator of the hammer damage. Actual damage will be 1/<number>*<health>.
    hammerextradamage       dJ      num     2       Denominator of the hammer damage while extra damage is on. Actual damage will be 1/<number>*<health>.
    
    <<< UTILITY EFFECTS >>>
    buildrange              J       %*      256     Max. building range of girder.
    extratime               e       time    30s     Time you get with Extra Time utility.
    lowgravity              l       %       50      Percentage of base gravity used when low gravity utility is used.
    timeboxturns            ut      num*    N/A     Time Box will return after this many additional turns. See parameter notes for details.
    pickhammerstraightdown  up      truth   false   If true, the pick hammer will be able to dig straight down only and it direction can not be changed.
    buildlimit              ub      num*    inf     How many constructions, rubbers and land sprays can be used per turn (inf = no limit).
    resurrectorrange        9       %       100     Range of resurrector in pixels. EXPERIMENTAL!
    
    <<< EYE CANDY / GIMMICKS >>>
    shoppaborder            o       truth   false   Landscape has a decorative “danger” stripe on its edges.
    ropecolor               xr      hex     D8D8D8  Rope color (will slightly change rope style). Use value “clan” to make rope appear in clan color.
    ropeopacity             xR      %       255     Opacity of ropes (will slighty change rope style). 255=full opacity.
    
    <<< SCRIPT CONFIGURATION >>>
    forcelimits             Z       truth   true    Cap most number-based values at reasonable bounds. WARNING: If false, correct operation is NOT guaranteed
    hedgepot                H       truth   false   Change 1-5 random game settings and 0-1 weapons/utilities.

### Parameter notes

Here are some detailed notes about specific parameters.

- `strategictools`
	- Disables infinite attack mode.
- `seductionrange`, `resurrectorrange`
	- Two circles will be drawn.
	- The brighter shows the actual range, the other circle is the original range
- `showdudminehealth`
	- Dud mine health is shown above the object
- `showminetimer`
	- Timer of mines are shown at the bottom right
	- Remember that the timer is rounded up! This means that a “1” can mean anything between 1 millisecond and 1 second
	- The color has a meaning:
		- white/yellow: Mine is inactive
			- white: Mine time is higher than 0.5s
			- yellow: Mine timer is 0.5s or lower
		- red: mine is active, the red tone tells you more about the timer time
			- brighter red: the upper half of the second (e.g. 0.5-1.0s)
			- darker red: the lower half of the second (e.g. 0.0-0.5s)
- `girders`
	- Constructions will be placed with a small buffer zone around them to avoid girders going to close to things and each other
	- Hedgehogs and objects will NOT spawn on the girders; girders are inserted after everything else has been generated and placed
	- If the girders would become too crammed in the landscapes, Game Hacks decides to stop adding more girders
	- With `forcelimits=false`, the above limit does not apply, but the loading time might increase considerably for a huge number of girders (>100000)
	- Known quirks:
		- Possibly poor girder placement for custom and “unorthodox” girder shapes from a theme
		- Considered experimental, the behaviour of this parameter may change in future
- `planedrops`, `planedropgap`
	- If you additionally explicitly specify parameters for the explicit plane drops, (`airattackbombs`, `airattackgap`, etc.) those explicit values take precedence
- `gravity`, `lowgravity`
	- Low gravity utility will modify gravity based on the base gravity
	- Disables built-in low gravity game modifier
	- For using high gravities, the turns may take longer to finish. This is mostly a problem in Hedgewars
- `forcefield`
	- The center of the map will have a barrier which prevents hedgehogs from going through
		- Exceptions: Kamikaze and Piano Strike (since those are attacks)
		- Weapons can go through
		- Ropes and portals can not go through
	- This feature is a bit buggy
	- The following things will be disabled:
		- Wrap world edge
		- Time Box
- `hogfriction`
	- Frictions determines how “well” the hogs slide, low friction means more sliding power
	- Note that this scale is not linear
	- Use these values as guide:
		- `9989`: max. friction (hedgehogs don't slide at all)
		- `20`: very high friction
		- `10`: high friction
		- `0`: default friction
		- `-5`: low friction
		- `-8`: very low friction
		- `-10`: extremely low friction
	- Use values in-between for fine-tuning
- `fairwind`, `maxwind`
	- Overwrites the built-in “disable wind” game modifier
- `minetimer`:
	- Will overwrite the default mine timer in the game scheme editor
	- Air mine timers will default to 750ms if not specified with airminetimer
	- The built-in mine timer setting will also affect the air mine timer
	- Use this setting when you need precision of 1 millisecond
	- Use the built-in setting when you need precision of 1 second only
	- Only use this key when you really need it
- `poisoncloudtimer`
	- Set `forcelimits` to `false` if you need to use timers longer than 5s.
	- However: Times above 5s are glitchy, the color and transparency of the poison cloud become awkward.
	- Use a time of 0 for a poison-free explosion of the old limburger.
- `barrelhealth`
	- Barrels will start frozen if health is above 60
- `hogfreeze`
	- The default value makes a hog skip 3 rounds (if it is not burned)
	- Each round reduces freeze level by 50000
	- Each time a flame is touched, freeze level is reduced by 1000
	- Example values:
		- melt immediately:   0
		- melt next turn: 50000
		- skip 1 round:  100000
		- skip 2 rounds: 150000
		- skip 3 rounds: 200000
		- and so on ...
- `sd1hp`, `sdpoison`
	- Be aware that the normal Sudden Death health decrease can still apply on top of these effects. It is recommended (but not mandatory) to set it to 0
	- If both Sudden Death health decrease and water rise are set to 0, the Sudden Death effect will be faked by the script but the background won't change
- `sharehealth`:
	- Health is shared as equally as possible. If the division leaves a remainder, the remainder will be split among some of the hedgehogs, beginning with the current hog and continuing with the next ones
- `healthcratehealth`
	- If this is set, the health crate amount set in the game scheme will be ignored
- `davidandgoliath`:
	- If the division leaves a remainder, the result is rounded down
	- Does not work well together with Reset Health mode: (health is reset to initial health in game scheme, rather than actual initial health)
- `donorbox`:
	- The donor box will replace the grave
- `timeboxturns`:
	- With value 0, the Time Box returns as fast as possible which will be in the next turn, turning the Time Box basically into a random teleportation
	- Value 1 refers to the turn after the next turn, and so on.
	- With the value “inf”, the Time Box will return only (!) in Sudden Death or when this is the final hog in the team, not earlier
	- Regardless of settings, the Time Box follows the standard Time Box behaviour of always returning in Sudden Death or when it is the final hog in the team

## Examples

In this section there are some examples for valid texts to put into the script parameter.

    saucerfuel=4000

--> The flying saucer has 200% fuel.

    saucerfuel=200%

--> The flying saucer has 200% fuel.

    saucerfuel=inf

--> The flying saucer has infinite fuel.

    stickyminetimer=0, nojump=true

--> Sticky mines explode instantly and jumping is disabled.

    minetimer=1s-2s

--> Mines have a random time which may be between 1 and 2 seconds long.

    ready=10s

--> The ready timer is set to 10 seconds.

    poison=true, poisondamage=10

--> All hedgehogs are poisoned from the beginning and take 10 poison damage per turn.

    gravity=200

--> The base gravity is doubled. It will go down to 100% when low gravity utility is used in-game (50% of base gravity).

    gravity=200%, lowgravity=25%

--> The base gravity is doubled. It will go down to 25% of that base gravity (!) when the low gravity utility is used, which is 50% of the default gravity.

    g=200%,l=25%

--> Same as the previous example, but it uses the short form.

    ropecolor=FF0000

--> All ropes are red.


## Licensing

Game Hacks is free software.

It's licensed under the MIT License. The media files (images) are based on Hedgewars content and are licensed under the GNU Free Documentation License 1.2.

See the `Game_Hacks_LICENSE_v21.txt` file for the full license texts.
